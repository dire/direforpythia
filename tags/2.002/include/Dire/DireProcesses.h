
// Pythia includes.
#include "Pythia8/Pythia.h"
#include "Pythia8/SigmaQCD.h"
#include "Pythia8/SigmaEW.h"

#include "Dire/Basics.h"
#include "Dire/WeightContainer.h"
#include "Dire/Hooks.h"

namespace Pythia8 {

class DireSigmaHelper {

public:

  DireSigmaHelper() : isInit(false), usePDFalphas(false), hasExternalHook(false),
    canSetMUF(false), canSetMUR(false), canSetMUQ(false), renormMultFac(1.0),
    factorMultFac(1.0), infoPtr(0), settingsPtr(0), particleDataPtr(0),
    beamAPtr(0), beamBPtr(0), couplingsPtr(0), weightsPtr(0), hooksPtr(0) {}

  double muRsq( double x1, double x2, double sH, double tH, double uH, 
    bool massless, double m1sq, double m2sq, double m3sq, double m4sq);
  double muFsq( double x1, double x2, double sH, double tH, double uH, 
    bool massless, double m1sq, double m2sq, double m3sq, double m4sq);
  double muQsq( double x1, double x2, double sH, double tH, double uH, 
    bool massless, double m1sq, double m2sq, double m3sq, double m4sq);

  double alpS( double mu2R) {
    double ret = couplingsPtr->alphaS(mu2R); 
    if (usePDFalphas) ret
      = (beamAPtr != 0) ? beamAPtr->alphaS(mu2R)
                        : (beamBPtr != 0) ? beamBPtr->alphaS(mu2R)
                                          : ret;
    return ret;
  }

  void init(Info* infoPtrIn, Settings* settingsPtrIn,
    ParticleData* particleDataPtrIn, Rndm*,
    BeamParticle* beamAPtrIn, BeamParticle* beamBPtrIn,
    Couplings* couplingsPtrIn, SusyLesHouches*,
    DireWeightContainer* weightsPtrIn = 0, DireHooks* hooksPtrIn = 0);

  bool isInit, usePDFalphas, hasExternalHook, canSetMUF, canSetMUR, canSetMUQ;
  double renormMultFac, factorMultFac;

  Info* infoPtr;
  Settings* settingsPtr;
  ParticleData* particleDataPtr;
  BeamParticle* beamAPtr;
  BeamParticle* beamBPtr;
  Couplings* couplingsPtr;

  DireWeightContainer* weightsPtr;
  DireHooks* hooksPtr;

};

//==========================================================================
 
class DireSigma2gg2gg : public Pythia8::Sigma2gg2gg {
public:
  DireSigma2gg2gg() { sigmaHelperPtr = 0; }
  void store2Kin( double x1in, double x2in, double sHin,
    double tHin, double m3in, double m4in, double runBW3in,
    double runBW4in);
  void setHelper( DireSigmaHelper* sigmaHelperPtrIn) {
    sigmaHelperPtr = sigmaHelperPtrIn;
  }
  DireSigmaHelper* sigmaHelperPtr;
  
};

//==========================================================================
 
class DireSigma2qqbar2qqbarNew : public Pythia8::Sigma2qqbar2qqbarNew {
public:
  DireSigma2qqbar2qqbarNew() { sigmaHelperPtr = 0; }
  void store2Kin( double x1in, double x2in, double sHin,
    double tHin, double m3in, double m4in, double runBW3in,
    double runBW4in);
  void setHelper( DireSigmaHelper* sigmaHelperPtrIn) {
    sigmaHelperPtr = sigmaHelperPtrIn;
  }
  DireSigmaHelper* sigmaHelperPtr;
  
};

//==========================================================================
 
class DireSigma2qqbar2gg : public Pythia8::Sigma2qqbar2gg {
public:
  DireSigma2qqbar2gg() { sigmaHelperPtr = 0; }
  void store2Kin( double x1in, double x2in, double sHin,
    double tHin, double m3in, double m4in, double runBW3in,
    double runBW4in);
  void setHelper( DireSigmaHelper* sigmaHelperPtrIn) {
    sigmaHelperPtr = sigmaHelperPtrIn;
  }
  DireSigmaHelper* sigmaHelperPtr;
  
};

//==========================================================================
 
class DireSigma2qq2qq : public Pythia8::Sigma2qq2qq {
public:
  DireSigma2qq2qq() { sigmaHelperPtr = 0; }
  void store2Kin( double x1in, double x2in, double sHin,
    double tHin, double m3in, double m4in, double runBW3in,
    double runBW4in);
  void setHelper( DireSigmaHelper* sigmaHelperPtrIn) {
    sigmaHelperPtr = sigmaHelperPtrIn;
  }
  DireSigmaHelper* sigmaHelperPtr;
  
};

//==========================================================================
 
class DireSigma2qg2qg : public Pythia8::Sigma2qg2qg {
public:
  DireSigma2qg2qg() { sigmaHelperPtr = 0; }
  void store2Kin( double x1in, double x2in, double sHin,
    double tHin, double m3in, double m4in, double runBW3in,
    double runBW4in);
  void setHelper( DireSigmaHelper* sigmaHelperPtrIn) {
    sigmaHelperPtr = sigmaHelperPtrIn;
  }
  DireSigmaHelper* sigmaHelperPtr;
  
};

//==========================================================================
 
class DireSigma2gg2qqbar : public Pythia8::Sigma2gg2qqbar {
public:
  DireSigma2gg2qqbar() { sigmaHelperPtr = 0; }
  void store2Kin( double x1in, double x2in, double sHin,
    double tHin, double m3in, double m4in, double runBW3in,
    double runBW4in);
  void setHelper( DireSigmaHelper* sigmaHelperPtrIn) {
    sigmaHelperPtr = sigmaHelperPtrIn;
  }
  DireSigmaHelper* sigmaHelperPtr;
  
};

//==========================================================================
 
class DireSigma2gg2QQbar : public Pythia8::Sigma2gg2QQbar {
public:
  DireSigma2gg2QQbar(int in1, int in2) : Pythia8::Sigma2gg2QQbar(in1,in2)
    { sigmaHelperPtr = 0; }
  void store2Kin( double x1in, double x2in, double sHin,
    double tHin, double m3in, double m4in, double runBW3in,
    double runBW4in);
  void setHelper( DireSigmaHelper* sigmaHelperPtrIn) {
    sigmaHelperPtr = sigmaHelperPtrIn;
  }
  DireSigmaHelper* sigmaHelperPtr;
  
};

//==========================================================================
 
class DireSigma2qqbar2QQbar : public Pythia8::Sigma2qqbar2QQbar {
public:
  DireSigma2qqbar2QQbar(int in1, int in2) : Pythia8::Sigma2qqbar2QQbar(in1,in2)
    { sigmaHelperPtr = 0; }
  void store2Kin( double x1in, double x2in, double sHin,
    double tHin, double m3in, double m4in, double runBW3in,
    double runBW4in);
  void setHelper( DireSigmaHelper* sigmaHelperPtrIn) {
    sigmaHelperPtr = sigmaHelperPtrIn;
  }
  DireSigmaHelper* sigmaHelperPtr;
  
};

//==========================================================================
 
class DireSigmaSigma2ff2fft_neutral_current : public Pythia8::Sigma2ff2fftgmZ {
public:
  DireSigmaSigma2ff2fft_neutral_current() { sigmaHelperPtr = 0; }
  void store2Kin( double x1in, double x2in, double sHin,
    double tHin, double m3in, double m4in, double runBW3in,
    double runBW4in);
  void setHelper( DireSigmaHelper* sigmaHelperPtrIn) {
    sigmaHelperPtr = sigmaHelperPtrIn;
  }
  DireSigmaHelper* sigmaHelperPtr;

  void initProc();
  void sigmaKin(); 
  double sigmaHat();
  void setIdColAcol(); 
  string name()   const {return "f_1 f_2 -> f_3 f_4 (Zp current)";}

private:

  //  Z parameters for propagator.
  int    gmZmode;
  double mZ, mZS, thetaWRat, sigmagmgm, sigmagmZ, sigmaZZ;

};

//==========================================================================
 
class DireSigmaSigma2ff2fft_charged_current : public Pythia8::Sigma2ff2fftW {
public:
  DireSigmaSigma2ff2fft_charged_current() { sigmaHelperPtr = 0; }
  void store2Kin( double x1in, double x2in, double sHin,
    double tHin, double m3in, double m4in, double runBW3in,
    double runBW4in);
  void setHelper( DireSigmaHelper* sigmaHelperPtrIn) {
    sigmaHelperPtr = sigmaHelperPtrIn;
  }
  DireSigmaHelper* sigmaHelperPtr;

  void initProc();
  void sigmaKin(); 
  double sigmaHat();
  void setIdColAcol(); 
  string name()   const {return "f_1 f_2 -> f_3 f_4 (Wp current)";}

private:

  //  W parameters for propagator.
  double mW, mWS, thetaWRat, sigma0;
 
};

}
