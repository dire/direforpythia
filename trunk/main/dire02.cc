
// DIRE includes.
#include "Dire/Dire.h"

// Pythia includes.
#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/HepMC2.h"

using namespace Pythia8;

static const double LARGE_WT = 5e2;

//==========================================================================

int main( int argc, char* argv[] ){

  // Check that correct number of command-line arguments
  if (argc < 3) {
    cerr << " Unexpected number of command-line arguments ("<<argc-1<<"). \n"
         << " You are expected to provide the arguments" << endl
         << " 1. Input file for settings" << endl
         << " 2. Output HepMC file" << endl
         << argc-1 << " arguments provided:";
         for ( int i=1; i<argc; ++i) cerr << " " << argv[i];
         cerr << "\n Program stopped. " << endl;
    return 1;
  }

  Pythia pythia;

  // Create DIRE shower plugin.
  Dire dire;

  // Interface for conversion from Pythia8::Event to HepMC one. 
  HepMC::Pythia8ToHepMC ToHepMC;
  // Specify file where HepMC events will be stored.
  HepMC::IO_GenEvent ascii_io(argv[2], std::ios::out);
  // Switch off warnings for parton-level events.
  ToHepMC.set_print_inconsistency(false);
  ToHepMC.set_free_parton_exception(false);
  // Do not store cross section information, as this will be done manually.
  ToHepMC.set_store_pdf(false);
  ToHepMC.set_store_proc(false);
  ToHepMC.set_store_xsec(false);

  // Create and open file for LHEF 3.0 output.
  //int pos = string(argv[2]).find(".");
  //string lhef_output = string(argv[2]).substr(0,pos+1) + "lhef";
  //LHEF3FromPythia8* lhef3 = NULL;

  // Initialize DIRE shower plugin.
  dire.init(pythia, argv[1], 0);

  // Get number of subruns.
  int nMerge = pythia.mode(" Main:numberOfSubruns");
  int nEvent = pythia.settings.mode("Main:numberOfEvents");
  bool fsr = pythia.flag("PartonLevel:FSR");
  bool isr = pythia.flag("PartonLevel:ISR");
  bool mpi = pythia.flag("PartonLevel:MPI");
  bool had = pythia.flag("HadronLevel:all");
  bool rem = pythia.flag("PartonLevel:Remnants");
  bool chk = pythia.flag("Check:Event");

  // Cross section estimate run.
  vector<double> sumSH;
  vector<double> nASH;
  vector<double> xs;
  vector<int> neve_real;
  // Loop over subruns with varying number of jets.
  for (int iMerge = 0; iMerge < nMerge; ++iMerge) {

    dire.resetFlag("PartonLevel:FSR",false);
    dire.resetFlag("PartonLevel:ISR",false);
    dire.resetFlag("PartonLevel:MPI",false);
    dire.resetFlag("HadronLevel:all",false);
    dire.resetFlag("PartonLevel:Remnants",false);
    dire.resetFlag("Check:Event",false);
    if (dire.doMerge()) dire.resetFlag("Merging:doXSectionEstimate", true);

    // Initialize with the LHE file for the current subrun.
    dire.init(pythia, argv[1], iMerge);

    int nEventEst = pythia.settings.mode("Main:numberOfEvents");
    // Cross section estimate run.
    double sumSHnow = 0.;
    double nAcceptSHnow = 0.;
    for( int iEvent=0; iEvent<nEventEst; ++iEvent ){
      // Generate next event
      if( !pythia.next() ) {
        if( pythia.info.atEndOfFile() )
          break;
        else continue;
      }
      sumSHnow     += pythia.info.weight();
      map <string,string> eventAttributes;
      if (pythia.info.eventAttributes)
        eventAttributes = *(pythia.info.eventAttributes);
      string trials = (eventAttributes.find("trials") != eventAttributes.end())
                  ?  eventAttributes["trials"] : "";
      if (trials != "") nAcceptSHnow += atof(trials.c_str());
    }

    // print cross section, errors
    pythia.stat();
    xs.push_back(pythia.info.sigmaGen());
    neve_real.push_back(
      (pythia.settings.flag("Merging:includeWeightInXsection")
      ? pythia.info.nSelected() : pythia.info.nAccepted()));
    sumSH.push_back(sumSHnow);
    nASH.push_back(nAcceptSHnow);
  }

  // Histogram the weight.
  Hist histWT("weight",10000,-500.,500.);
  Hist histWTMEPS("weight",10000,-500.,500.);
  Hist histT("t",50,0.,50.);

  // Switch showering and multiple interaction back on.
  pythia.settings.flag("PartonLevel:FSR",fsr);
  pythia.settings.flag("PartonLevel:ISR",isr);
  pythia.settings.flag("HadronLevel:all",had);
  pythia.settings.flag("PartonLevel:MPI",mpi);
  pythia.settings.flag("PartonLevel:Remnants",rem);
  pythia.settings.flag("Check:Event",chk);
  pythia.settings.flag("Merging:doXSectionEstimate", false);

  //if (lhef_output != "") {
  //  lhef3 = new LHEF3FromPythia8(&pythia.event,
  //     &pythia.settings, &pythia.info,
  //     &pythia.particleData);
  //   lhef3->openLHEF(lhef_output);
  //   // Write out initialization info on the file.
  //   lhef3->setInit();
  //}

  // Histogram the weight.
  // Cross section and error.
  double sigmaTotal  = 0.;
  double errorTotal  = 0.;

  // Loop over subruns with varying number of jets.
  for (int iMerge = 0; iMerge < nMerge; ++iMerge) {

    double sigmaSample = 0., errorSample = 0.;
    // Initialize with the LHE file for the current subrun.
    dire.init(pythia, argv[1], iMerge);

    double pswtmax =-1e15;
    double pswtmin = 1e15;
    double sumpswt = 0.;
    double sumpswtsq = 0.;
    double mepswtmax =-1e15;
    double mepswtmin = 1e15;
    double summepswt = 0.;
    double summepswtsq = 0.;
    int na=0;

    // Get the inclusive x-section by summing over all process x-sections.
    //double xs = 0.;
    //for (int i=0; i < pythia.info.nProcessesLHEF(); ++i)
    //  xs += pythia.info.sigmaLHEF(i);

    // Start generation loop
    for ( int iEvent=0; iEvent<nEvent; ++iEvent ){

      // Generate next event
      if( !pythia.next() ) {
        if( pythia.info.atEndOfFile() )
          break;
        else continue;
      }

      // Get event weight(s).
      double meweight         = pythia.info.weight();
      double mepsweight       = pythia.info.mergingWeightNLO();
      mepswtmin = min(mepswtmin,mepsweight);
      mepswtmax = max(mepswtmax,mepsweight);
      summepswt += mepsweight;
      summepswtsq+=pow2(mepsweight);
      histWTMEPS.fill( mepsweight, 1.0);

      // Retrieve the shower weight.
      dire.weightsPtr->calcWeight(0.);
      dire.weightsPtr->reset();
      double psweight = dire.weightsPtr->getShowerWeight();
      pswtmin = min(pswtmin,psweight);
      pswtmax = max(pswtmax,psweight);
      sumpswt += psweight;
      sumpswtsq+=pow2(psweight);
      histWT.fill( psweight, 1.0);

      double wt = meweight*psweight*mepsweight;

      if (abs(psweight) > LARGE_WT) {
        cout << scientific << setprecision(8)
        << "Warning in DIRE main program dire02.cc: Large shower weight wt="
        << psweight
        << ". Discard event with rare shower weight fluctuation."
        << endl;
        meweight = psweight = mepsweight = wt = 0.;
      }

      if (abs(mepsweight) > LARGE_WT) {
        cout << scientific << setprecision(8)
        << "Warning in DIRE main program dire02.cc: Large merging weight wt="
        << mepsweight
        << ". Discard event with rare merging weight fluctuation."
        << endl;
        meweight = psweight = mepsweight = wt = 0.;
      }

//      // Do not print zero-weight events.
//      if ( meweight  == 0. )  continue;
//      if ( mepsweight == 0. ) continue;

      /*// Work with weighted (LHA strategy=-4) events.
      double normhepmc = 1.;
      if (abs(pythia.info.lhaStrategy()) == 4)
        normhepmc = 1. / double(1e9*nEvent);
      // Work with unweighted events.
      else
        normhepmc = xs / double(1e9*nEvent);*/

      double normhepmc = xs[iMerge] / double(neve_real[iMerge]);
      // Weighted events with additional number of trial events to consider.
      if ( pythia.info.lhaStrategy() != 0
        && pythia.info.lhaStrategy() != 3
        && nASH[iMerge] > 0)
        normhepmc = 1. / (1e9*nASH[iMerge]);
      // Weighted events.
      else if ( pythia.info.lhaStrategy() != 0
        && pythia.info.lhaStrategy() != 3
        && nASH[iMerge] == 0)
        normhepmc = 1. / (1e9*neve_real[iMerge]);

      int neve = dire.direInfo.eventNumber++;
      pythia.info.setEventAttribute("eventNumber",
        to_string(neve));
      ostringstream sstream;
      sstream << scientific << setprecision(8);
      sstream << wt*normhepmc;
      pythia.info.setEventAttribute("unlops_weight", sstream.str());
      pythia.info.setEventAttribute("t", pythia.info.getEventAttribute("t1"));
      pythia.info.setEventAttribute("z", pythia.info.getEventAttribute("z1"));
      pythia.info.setEventAttribute("phi", pythia.info.getEventAttribute("phi1"));
      //pythia.info.removeEventAttribute("lastpt");
      //pythia.info.removeEventAttribute("npLO");
      //pythia.info.removeEventAttribute("npNLO");
      //pythia.info.removeEventAttribute("zsgn");

      // Construct new empty HepMC event.
      HepMC::GenEvent* hepmcevt = new HepMC::GenEvent();
      // Set event weight
      hepmcevt->weights().push_back(wt*normhepmc);
      /*hepmcevt->weights()["Weight"] = wt*normhepmc;
      for ( map<string, string>::iterator
        it  = (*pythia.info.eventAttributes).begin();
        it != (*pythia.info.eventAttributes).end(); ++it)
        hepmcevt->weights()[it->first] = atof(it->second.c_str());*/

      // Fill HepMC event
      ToHepMC.fill_next_event( pythia, hepmcevt );

      // Add the weight of the current event to the cross section. 
      sigmaTotal  += wt*normhepmc;
      sigmaSample += wt*normhepmc;
      errorTotal  += pow2(wt*normhepmc);
      errorSample += pow2(wt*normhepmc);

      // Report cross section to hepmc
      HepMC::GenCrossSection xsec;
      xsec.set_cross_section( sigmaTotal*1e9, pythia.info.sigmaErr()*1e9 );
      hepmcevt->set_cross_section( xsec );
      // Write the HepMC event to file. Done with it.
      ascii_io << hepmcevt;
      delete hepmcevt;

      // Store and write event info.
      //if (lhef3) lhef3->setEvent();

      double t1 = atof(pythia.info.getEventAttribute("t1").c_str());
      //double t2 = atof(pythia.info.getEventAttribute("t2").c_str());
      //double t3 = atof(pythia.info.getEventAttribute("t3").c_str());
      histT.fill(t1, wt*normhepmc);

      if ( meweight  != 0. && mepsweight != 0. ) na++;

    } // end loop over events to generate

    // print cross section, errors
    pythia.stat();

    cout << endl << " Contribution of sample " << iMerge
       //<< " " << pythia.settings.word("Beams:LHEF")
       << " to the inclusive cross section : "
       << scientific << setprecision(8)
       << sigmaSample << "  +-  " << sqrt(errorSample)
       << " veto rate=" << (1.-double(na)/double(nEvent)) << endl;

    cout
       << "\t npLO=" << pythia.info.getEventAttribute("npLO",true)
       << " npNLO="   << pythia.info.getEventAttribute("npNLO",true)
       << " Minimal shower weight=" << pswtmin
       << "\n\t npLO=" << pythia.info.getEventAttribute("npLO",true)
       << " npNLO="   << pythia.info.getEventAttribute("npNLO",true)
       << " Maximal shower weight=" << pswtmax
       << "\n\t Mean shower weight=" << sumpswt/double(nEvent)
       << "\n\t Variance of shower weight="
       << sqrt(1/double(nEvent)*(sumpswtsq - pow(sumpswt,2)/double(nEvent)))
       << "\n\n"
       << "\t npLO=" << pythia.info.getEventAttribute("npLO",true)
       << " npNLO="   << pythia.info.getEventAttribute("npNLO",true)
       << " Minimal meps weight=" << mepswtmin
       << "\n\t npLO=" << pythia.info.getEventAttribute("npLO",true)
       << " npNLO="   << pythia.info.getEventAttribute("npNLO",true)
       << " Maximal meps weight=" << mepswtmax
       << "\n\t Mean meps weight=" << summepswt/double(nEvent)
       << "\n\t Variance of meps weight="
       << sqrt(1/double(nEvent)*(summepswtsq - pow(summepswt,2)/double(nEvent)))
       << endl;
  }

  // Write endtag. Overwrite initialization info with new cross sections.
  //if (lhef3) lhef3->closeLHEF(true);

  /*ofstream write;
  // Write histograms to file
  write.open("wt.dat");
  histWT.table(write);
  write.close();
  write.open("wtmeps.dat");
  histWTMEPS.table(write);
  write.close();
  write.open("t.dat");
  histT.table(write);
  write.close();*/

  cout << endl << endl << endl;
  cout << "Inclusive cross section: " << scientific << setprecision(8)
         << sigmaTotal << "  +-  " << sqrt(errorTotal) << " mb " << endl;
  cout << endl << endl << endl;

  cout << histT << endl;

  // Done
  return 0;

}
