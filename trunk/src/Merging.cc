// Merging.cc is a part of the DIRE plugin to the PYTHIA event generator.
// Copyright (C) 2018 Stefan Prestel.

#include "Dire/Merging.h"
#include "Dire/DireSpace.h"
#include "Dire/DireTimes.h"

namespace Pythia8 {

//==========================================================================

// The Merging class.

//--------------------------------------------------------------------------


// Check colour/flavour correctness of state.

bool validEvent( const Event& event ) {

  bool validColour  = true;
  bool validCharge  = true;
  bool validMomenta = true;
  double mTolErr=1e-2;

  // Check charge sum in initial and final state
  double initCharge = event[3].charge() + event[4].charge();
  double finalCharge = 0.0;
  for(int i = 0; i < event.size(); ++i)
    if (event[i].isFinal()) finalCharge += event[i].charge();
  if (abs(initCharge-finalCharge) > 1e-12) validCharge = false;

  // Check that overall pT is vanishing.
  Vec4 pSum(0.,0.,0.,0.);
  for ( int i = 0; i < event.size(); ++i) {
    //if ( i ==3 || i == 4 )    pSum -= event[i].p();
    if ( event[i].status() == -21 ) pSum -= event[i].p();
    if ( event[i].isFinal() )       pSum += event[i].p();
  }
  if ( abs(pSum.px()) > mTolErr || abs(pSum.py()) > mTolErr) {
    validMomenta = false;
  }

  if ( event[3].status() == -21
    && (abs(event[3].px()) > mTolErr || abs(event[3].py()) > mTolErr)){
    validMomenta = false;
  }
  if ( event[4].status() == -21
    && (abs(event[4].px()) > mTolErr || abs(event[4].py()) > mTolErr)){
    validMomenta = false;
  }

  return (validColour && validCharge && validMomenta);

}

//--------------------------------------------------------------------------

// Initialise Merging class

void DireMerging::init(){
  // Reset minimal tms value.
  tmsNowMin             = infoPtr->eCM();

  if (!myLHEF3Ptr) myLHEF3Ptr = new LHEF3FromPythia8(0, settingsPtr,
    infoPtr, particleDataPtr, 15, false);

  enforceCutOnLHE = settingsPtr->flag("Merging:enforceCutOnLHE");
  doMOPS          = settingsPtr->flag("Dire:doMOPS");
  applyTMSCut     = settingsPtr->flag("Merging:doXSectionEstimate");
  doMerging       = settingsPtr->flag("Dire:doMerging");
  usePDF          = settingsPtr->flag("ShowerPDF:usePDF");
  usePDFmass      = settingsPtr->flag("ShowerPDF:usePDFmasses")
                 && (toLower(settingsPtr->word("PDF:pSet")).find("lhapdf")
                          != string::npos);
  allowReject     = settingsPtr->flag("Merging:applyVeto");
  doMECs          = settingsPtr->flag("Dire:doMECs");
  doMEM           = settingsPtr->flag("Dire:doMEM");
  doGenerateSubtractions   = settingsPtr->flag("Dire:doGenerateSubtractions");
  doGenerateMergingWeights = settingsPtr->flag("Dire:doGenerateMergingWeights");
  doExitAfterMerging       =  settingsPtr->flag("Dire:doExitAfterMerging");
  allowIncompleteReal
    =  settingsPtr->flag("Merging:allowIncompleteHistoriesInReal");
  nQuarksMerge    =  settingsPtr->mode("Merging:nQuarksMerge");

  doWeakMerging   =  settingsPtr->flag("SpaceShower:EWshowerByQ")
                  || settingsPtr->flag("TimeShower:EWshowerByQ");
  first = true;
  ntries=0;

  // Cross section conversion factor to undo stupid conversions done in
  // info.weight() calls.
  CONVERTMB2PB = 1.;
  if (abs(infoPtr->lhaStrategy()) == 4) CONVERTMB2PB = 1e9;

}

//--------------------------------------------------------------------------

// Function to print information.
void DireMerging::statistics() {

  // Recall merging scale value.
  double tmsval         = mergingHooksPtr->tms();
  bool printBanner      = enforceCutOnLHE && tmsNowMin > TMSMISMATCH*tmsval
                        && tmsval > 0.;
  // Reset minimal tms value.
  tmsNowMin             = infoPtr->eCM();

  if (doMOPS) printBanner = false;
  if (doMEM)  printBanner = false;
  if (doMECs) printBanner = false;

  if (!printBanner) return;

  // Header.
  cout << "\n *-------  PYTHIA Matrix Element Merging Information  ------"
       << "-------------------------------------------------------*\n"
       << " |                                                            "
       << "                                                     |\n";
  // Print warning if the minimal tms value of any event was significantly
  // above the desired merging scale value.
  cout << " | Warning in DireMerging::statistics: All Les Houches events"
       << " significantly above Merging:TMS cut. Please check.       |\n";

  // Listing finished.
  cout << " |                                                            "
       << "                                                     |\n"
       << " *-------  End PYTHIA Matrix Element Merging Information -----"
       << "-----------------------------------------------------*" << endl;
}

//--------------------------------------------------------------------------

// Return event stripped from decay products.

Event DireMerging::insertResonances (const Event& inputEventIn) {

  // Find and detach decay products.
  Event newProcess = Event();
  newProcess.init("(hard process-modified)", particleDataPtr);

  // If there are already intermediate resonances, simply return
  // event w/o decay products.
  bool hasRes = false;
  for (int i = 0; i < inputEventIn.size(); ++ i)
    if ( inputEventIn[i].statusAbs() == 22) hasRes = true;
  if (hasRes) return inputEventIn;

  // Add the beam and initial partons to the event record.
  for (int i = 0; i < inputEventIn.size(); ++ i) {
    if ( inputEventIn[i].mother1() > 4
      || inputEventIn[i].statusAbs() == 22
      || inputEventIn[i].statusAbs() == 23)
      break;
    newProcess.append(inputEventIn[i]);
  }

  Vec4 pV; int chg(0);
  for (int i = 0; i < inputEventIn.size(); ++ i) {
    if (inputEventIn[i].mother1() > 4) break;
    if ( inputEventIn[i].statusAbs() != 11
      && inputEventIn[i].statusAbs() != 12
      && inputEventIn[i].statusAbs() != 21
      && inputEventIn[i].statusAbs() != 22
      && inputEventIn[i].isLepton()) {
      if (inputEventIn[i].isCharged() && inputEventIn[i].charge() < 0) chg--;
      if (inputEventIn[i].isCharged() && inputEventIn[i].charge() > 0) chg++;
      pV += inputEventIn[i].p();
    }
  }

  int idRes = 23;
  if (chg == 0)  idRes =  23;
  if (chg == -1) idRes = -24;
  if (chg == +1) idRes =  24;

  int iRes = newProcess.append(idRes,-22,3,4,0,0,0,0,
    pV, pV.mCalc(), pV.mCalc());

  // Add outgoing particles to the event record.
  int iMax(0), iMin(iRes);
  for (int i = 0; i < inputEventIn.size(); ++ i) {
    if (inputEventIn[i].mother1() > 4) break;
    if ( inputEventIn[i].statusAbs() != 11
      && inputEventIn[i].statusAbs() != 12
      && inputEventIn[i].statusAbs() != 21
      && inputEventIn[i].statusAbs() != 22
      && !inputEventIn[i].isLepton()) {
      int iNow = newProcess.append(inputEventIn[i]);
      newProcess[iNow].mothers(3,4);
      iMax = max(iMax,iNow);
      iMin = min(iMin,iNow);
    }
  }

  int iMaxRes(0), iMinRes(1000);
  for (int i = 0; i < inputEventIn.size(); ++ i) {
    if (inputEventIn[i].mother1() > 4) break;
    if ( inputEventIn[i].statusAbs() != 11
      && inputEventIn[i].statusAbs() != 12
      && inputEventIn[i].statusAbs() != 21
      && inputEventIn[i].statusAbs() != 22
      && inputEventIn[i].isLepton()) {
      int iNow = newProcess.append(inputEventIn[i]);
      newProcess[iNow].mothers(iRes,0);
      iMaxRes = max(iMaxRes,iNow);
      iMinRes = min(iMinRes,iNow);
    }
  }
  newProcess[iRes].daughters(iMinRes,iMaxRes);
  newProcess[3].daughters(iMin,iMax);
  newProcess[4].daughters(iMin,iMax);

  // Update event colour tag to maximum in whole process.
  int maxColTag = 0;
  for (int i = 0; i < inputEventIn.size(); ++ i) {
    if ( inputEventIn[i].col() > maxColTag )
      maxColTag = inputEventIn[i].col();
    if ( inputEventIn[i].acol() > maxColTag )
      maxColTag = inputEventIn[i].acol();
  }
  newProcess.initColTag(maxColTag);

  // Copy junctions from process to newProcess.
  for (int i = 0; i < inputEventIn.sizeJunction(); ++i)
    newProcess.appendJunction( inputEventIn.getJunction(i));
  newProcess.saveSize();
  newProcess.saveJunctionSize();
  // Remember scale
  newProcess.scale( inputEventIn.scale() );

  // Done
  return newProcess;

}

//--------------------------------------------------------------------------

// Return event stripped from decay products.

Event DireMerging::stripResonances (const Event& inputEventIn) {

  // Find and detach decay products.
  Event newProcess = Event();
  newProcess.init("(hard process-modified)", particleDataPtr);

  // If there are no intermediate resonances, simply return event.
  bool hasRes = false;
  for (int i = 0; i < inputEventIn.size(); ++ i)
    if ( inputEventIn[i].statusAbs() == 22) hasRes = true;
  if (!hasRes) return inputEventIn;

  // Add the beam and initial partons to the event record.
  for (int i = 0; i < inputEventIn.size(); ++ i) {
    if ( inputEventIn[i].mother1() > 4
      || inputEventIn[i].statusAbs() == 22
      || inputEventIn[i].statusAbs() == 23)
      break;
    newProcess.append(inputEventIn[i]);
  }

  // Add outgoing particles to the event record.
  int iMax(0), iMin(10000);
  for (int i = 0; i < inputEventIn.size(); ++ i) {
    if ( inputEventIn[i].mother1() <= 4
      || inputEventIn[i].statusAbs() == 11
      || inputEventIn[i].statusAbs() == 12
      || inputEventIn[i].statusAbs() == 21
      || inputEventIn[i].statusAbs() == 22) continue;
    int iNow = newProcess.append(inputEventIn[i]);
    newProcess[iNow].mothers(3,4);
    iMax = max(iMax,iNow);
    iMin = min(iMin,iNow);
  }

  if (iMax>=iMin){
    newProcess[3].daughters(iMin,iMax);
    newProcess[4].daughters(iMin,iMax);
  }

  // Update event colour tag to maximum in whole process.
  int maxColTag = 0;
  for (int i = 0; i < inputEventIn.size(); ++ i) {
    if ( inputEventIn[i].col() > maxColTag )
      maxColTag = inputEventIn[i].col();
    if ( inputEventIn[i].acol() > maxColTag )
      maxColTag = inputEventIn[i].acol();
  }
  newProcess.initColTag(maxColTag);

  // Copy junctions from process to newProcess.
  for (int i = 0; i < inputEventIn.sizeJunction(); ++i)
    newProcess.appendJunction( inputEventIn.getJunction(i));
  newProcess.saveSize();
  newProcess.saveJunctionSize();
  // Remember scale
  newProcess.scale( inputEventIn.scale() );

  // Done
  return newProcess;

}

//--------------------------------------------------------------------------

void DireMerging::storeInfos() {

  // Clear previous information.
  clearInfos();

  // Store information on every possible last clustering.
  for ( int i = 0 ; i < int(myHistory->children.size()); ++i) {
    // Just store pT and mass for now.
    stoppingScalesSave.push_back(myHistory->children[i]->clusterIn.pT());
    radSave.push_back(myHistory->children[i]->clusterIn.radPos);
    emtSave.push_back(myHistory->children[i]->clusterIn.emt1Pos);
    recSave.push_back(myHistory->children[i]->clusterIn.recPos);
    mDipSave.push_back(myHistory->children[i]->clusterIn.mass());
  }

}

//--------------------------------------------------------------------------

void DireMerging::getStoppingInfo(double scales [100][100],
  double masses [100][100]) {

  int posOffest=2;
  for (unsigned int i=0; i < radSave.size(); ++i){
    scales[radSave[i]-posOffest][recSave[i]-posOffest] = stoppingScalesSave[i];
    masses[radSave[i]-posOffest][recSave[i]-posOffest] = mDipSave[i];
  }

}

//--------------------------------------------------------------------------

double DireMerging::generateSingleSudakov ( double pTbegAll, 
  double pTendAll, double m2dip, int idA, int type, double s, double x) {
  return isr->noEmissionProbability( pTbegAll, pTendAll, m2dip, idA,
    type, s, x);
}

//--------------------------------------------------------------------------

void DireMerging::reset() {
  mergingHooksPtr->partonSystemsPtr->clear();
  isr->clear();
  fsr->clear();
  beamAPtr->clear();
  beamBPtr->clear();
}

//--------------------------------------------------------------------------

// Function to steer different merging prescriptions.

bool DireMerging::generateUnorderedPoint(Event& process){

  Event newProcess(evtUtils->makeHardEvent(0, process, false));
  bool found = false;

  // Dryrun to find overestimate enhancements if necessary.
  if (first) {

    isr->dryrun = true;
    fsr->dryrun = true;

    for (int iii=0; iii<50; ++iii) {

      int sizeOld = newProcess.size();
      int sizeNew = newProcess.size();
      int nSplit = 2;

      while (sizeNew < sizeOld+nSplit) {

        // Incoming partons to hard process are stored in slots 3 and 4.
        int inS = 0;
        int inP = 3;
        int inM = 4;
        int sizeProcess = newProcess.size();
        int sizeEvent   = newProcess.size();
        int nOffset  = sizeEvent - sizeProcess;
        int nHardDone = sizeProcess;
        double scale = newProcess.scale();
        // Store participating partons as first set in list of all systems.
        mergingHooksPtr->partonSystemsPtr->addSys();
        mergingHooksPtr->partonSystemsPtr->setInA(0, inP + nOffset);
        mergingHooksPtr->partonSystemsPtr->setInB(0, inM + nOffset);
        for (int i = inM + 1; i < nHardDone; ++i)
          mergingHooksPtr->partonSystemsPtr->addOut(0, i + nOffset);
        mergingHooksPtr->partonSystemsPtr->setSHat( 0,
          (newProcess[inP + nOffset].p() + newProcess[inM + nOffset].p()).m2Calc() );
        mergingHooksPtr->partonSystemsPtr->setPTHat( 0, scale);

        // Add incoming hard-scattering partons to list in beam remnants.
        double x1 = newProcess[inP].pPos() / newProcess[inS].m();
        double x2 = newProcess[inM].pNeg() / newProcess[inS].m();
        beamAPtr->append( inP + nOffset, newProcess[inP].id(), x1);
        beamBPtr->append( inM + nOffset, newProcess[inM].id(), x2);
        // Scale. Find whether incoming partons are valence or sea. Store.
        beamAPtr->xfISR( 0, newProcess[inP].id(), x1, scale*scale);
        int vsc1 = beamAPtr->pickValSeaComp();
        beamBPtr->xfISR( 0, newProcess[inM].id(), x2, scale*scale);
        int vsc2 = beamBPtr->pickValSeaComp();
        bool isVal1 = (vsc1 == -3);
        bool isVal2 = (vsc2 == -3);
        infoPtr->setValence( isVal1, isVal2);

        isr->prepare(0, newProcess, true);
        fsr->prepare(0, newProcess, true);

        Event in(newProcess);
        double pTnowFSR = fsr->newPoint(in);
        double pTnowISR = isr->newPoint(in);
        if (pTnowFSR==0. && pTnowISR==0.) { reset(); continue; }
        bool branched=false;
        if (pTnowFSR > pTnowISR) branched = fsr->branch(in);
        else                     branched = isr->branch(in);
        if (!branched) { reset(); continue; }
        if (pTnowFSR > pTnowISR) isr->update(0, in, false);
        else                     fsr->update(0, in, false);

        pTnowISR = isr->newPoint(in);
        pTnowFSR = fsr->newPoint(in);
        if (pTnowFSR==0. && pTnowISR==0.) { reset(); continue; }
        branched=false;
        if (pTnowFSR > pTnowISR) branched = fsr->branch(in);
        else                     branched = isr->branch(in);
        if (!branched) { reset(); continue; }
        if (pTnowFSR > pTnowISR) isr->update(0, in, false);
        else                     fsr->update(0, in, false);

        // Done. Clean up event and return.
        sizeNew = evtUtils->makeHardEvent(0, in, false).size();
        mergingHooksPtr->partonSystemsPtr->clear();
        isr->clear();
        fsr->clear();
        beamAPtr->clear();
        beamBPtr->clear();
      }
    }

  // Generate arbitrary phase-space point with two additional particles.
  } else {

    int sizeOld = newProcess.size();
    int sizeNew = newProcess.size();
    int nSplit = 2;

    while (sizeNew < sizeOld+nSplit) {

      ntries++;
      // Incoming partons to hard process are stored in slots 3 and 4.
      int inS = 0;
      int inP = 3;
      int inM = 4;
      int sizeProcess = newProcess.size();
      int sizeEvent   = newProcess.size();
      int nOffset  = sizeEvent - sizeProcess;
      int nHardDone = sizeProcess;
      double scale = newProcess.scale();
      // Store participating partons as first set in list of all systems.
      mergingHooksPtr->partonSystemsPtr->addSys();
      mergingHooksPtr->partonSystemsPtr->setInA(0, inP + nOffset);
      mergingHooksPtr->partonSystemsPtr->setInB(0, inM + nOffset);
      for (int i = inM + 1; i < nHardDone; ++i)
        mergingHooksPtr->partonSystemsPtr->addOut(0, i + nOffset);
      mergingHooksPtr->partonSystemsPtr->setSHat( 0,
        (newProcess[inP + nOffset].p() + newProcess[inM + nOffset].p()).m2Calc() );
      mergingHooksPtr->partonSystemsPtr->setPTHat( 0, scale);

      // Add incoming hard-scattering partons to list in beam remnants.
      double x1 = newProcess[inP].pPos() / newProcess[inS].m();
      double x2 = newProcess[inM].pNeg() / newProcess[inS].m();
      beamAPtr->append( inP + nOffset, newProcess[inP].id(), x1);
      beamBPtr->append( inM + nOffset, newProcess[inM].id(), x2);

      // Scale. Find whether incoming partons are valence or sea. Store.
      // When an x-dependent matter profile is used with nonDiffractive,
      // trial interactions mean that the valence/sea choice has already
      // been made and should be restored here.
      beamAPtr->xfISR( 0, newProcess[inP].id(), x1, scale*scale);
      int vsc1 = beamAPtr->pickValSeaComp();
      beamBPtr->xfISR( 0, newProcess[inM].id(), x2, scale*scale);
      int vsc2 = beamBPtr->pickValSeaComp();
      bool isVal1 = (vsc1 == -3);
      bool isVal2 = (vsc2 == -3);
      infoPtr->setValence( isVal1, isVal2);

      isr->prepare(0, newProcess, true);
      fsr->prepare(0, newProcess, true);

      Event in(newProcess);

      double pTnowFSR = fsr->newPoint(in);
      double pTnowISR = isr->newPoint(in);

      if (pTnowFSR==0. && pTnowISR==0.) { reset(); continue; }
      bool branched=false;
      if (pTnowFSR > pTnowISR) branched = fsr->branch(in);
      else                     branched = isr->branch(in);
      if (!branched) { reset(); continue; }
      if (pTnowFSR > pTnowISR) isr->update(0, in, false);
      else                     fsr->update(0, in, false);

      pTnowISR = isr->newPoint(in);
      pTnowFSR = fsr->newPoint(in);
      if (pTnowFSR==0. && pTnowISR==0.) { reset(); continue; }
      branched=false;
      if (pTnowFSR > pTnowISR) branched = fsr->branch(in);
      else                     branched = isr->branch(in);
      if (!branched) { reset(); continue; }
      //if (!branched) { reset(); continue; }
      if (pTnowFSR > pTnowISR) isr->update(0, in, false);
      else                     fsr->update(0, in, false);

      // Done. Clean up event and return.
      //in = fsr->makeHardEvent(0, in, false);
      in = evtUtils->makeHardEvent(0, in, false);
      reset();

      // Loop through event and count.
      int nPartons(0);
      for(int i=0; i < int(in.size()); ++i)
        if ( in[i].isFinal()
          && in[i].colType()!= 0
          && ( in[i].id() == 21 || in[i].idAbs() <= nQuarksMerge))
          nPartons++;
      nPartons -= mergingHooksPtr->hardProcess->nQuarksOut();

      // Set number of requested partons.
      settingsPtr->mode("Merging:nRequested", nPartons);
      mergingHooksPtr->nRequestedSave
        = settingsPtr->mode("Merging:nRequested");
      evtUtils->reattachDecaysAfterMerging(in);

      generateHistories(in, false);
      reset();
      //if (myHistory->foundAnyOrderedPaths()) continue;
      if (myHistory->foundAnyOrderedPaths()) infoPtr->updateWeight(0.);
      newProcess = in;

      sizeNew = newProcess.size();
      found = true;

    }
  }

  if (!found) {
    // Loop through event and count.
    int nPartons(0);
    for(int i=0; i < int(newProcess.size()); ++i)
      if ( newProcess[i].isFinal()
        && newProcess[i].colType()!= 0
        && ( newProcess[i].id() == 21 || newProcess[i].idAbs() <= nQuarksMerge))
        nPartons++;
    nPartons -= mergingHooksPtr->hardProcess->nQuarksOut();

    // Set number of requested partons.
    settingsPtr->mode("Merging:nRequested", nPartons);
    mergingHooksPtr->nRequestedSave
      = settingsPtr->mode("Merging:nRequested");
    evtUtils->reattachDecaysAfterMerging(newProcess);
  }
  process = newProcess;

  first=false;
  isr->dryrun=false;
  fsr->dryrun=false;

  return true;

}

//--------------------------------------------------------------------------

// Function to steer different merging prescriptions.

void DireMerging::initSettings(){

  // Reinitialise hard process.
  mergingHooksPtr->hardProcess->clear();
  string processNow = settingsPtr->word("Merging:Process");
  // Remove whitespace from process string
  while(processNow.find(" ", 0) != string::npos)
    processNow.erase(processNow.begin()+processNow.find(" ",0));
#if (PYTHIA_VERSION_INTEGER >= 8242)
  mergingHooksPtr->processNow  = processNow;
#endif
  mergingHooksPtr->hardProcess->initOnProcess(processNow, particleDataPtr);

  mergingHooksPtr->doUserMergingSave
    = settingsPtr->flag("Merging:doUserMerging");
  mergingHooksPtr->doMGMergingSave
    = settingsPtr->flag("Merging:doMGMerging");
  mergingHooksPtr->doKTMergingSave
    = settingsPtr->flag("Merging:doKTMerging");
  mergingHooksPtr->doPTLundMergingSave
    = settingsPtr->flag("Merging:doPTLundMerging");
  mergingHooksPtr->doCutBasedMergingSave
    = settingsPtr->flag("Merging:doCutBasedMerging");
  mergingHooksPtr->doUNLOPSTreeSave
    = settingsPtr->flag("Merging:doUNLOPSTree");
  mergingHooksPtr->doUNLOPSLoopSave
    = settingsPtr->flag("Merging:doUNLOPSLoop");
  mergingHooksPtr->doUNLOPSSubtSave
    = settingsPtr->flag("Merging:doUNLOPSSubt");
  mergingHooksPtr->doUNLOPSSubtNLOSave
    = settingsPtr->flag("Merging:doUNLOPSSubtNLO");
  mergingHooksPtr->doUMEPSTreeSave
    = settingsPtr->flag("Merging:doUMEPSTree");
  mergingHooksPtr->doUMEPSSubtSave
    = settingsPtr->flag("Merging:doUMEPSSubt");
  mergingHooksPtr->nReclusterSave
    = settingsPtr->mode("Merging:nRecluster");

  mergingHooksPtr->hasJetMaxLocal  = false;
  mergingHooksPtr->nJetMaxLocal
    = mergingHooksPtr->nJetMaxSave;
  mergingHooksPtr->nJetMaxNLOLocal
    = mergingHooksPtr->nJetMaxNLOSave;

}

//--------------------------------------------------------------------------

void DireMerging::initOnEvent(const Event& process){

  unitarizationFactor(1.);

  string processNow = settingsPtr->word("Merging:Process");

  // Reset to default merging scale.
  mergingHooksPtr->tms(mergingHooksPtr->tmsCut());

  int nPartons = 0;
  // Do not include resonance decay products in the counting.
  Event newp( evtUtils->eventForMerging( process, false) );

  // Call nSteps once to store some counters for inclusive merging.
  mergingHooksPtr->getNumberOfClusteringSteps( newp, true);

  // Dynamically set the process string.
  if ( processNow.find("guess") != string::npos) {
    string processString = "";
    // Set incoming particles.
    int beamAid = beamAPtr->id();
    int beamBid = beamBPtr->id();
    if (abs(beamAid) == 2212) processString += "p";
    if (beamAid == 11)        processString += "e-";
    if (beamAid ==-11)        processString += "e+";
    if (abs(beamBid) == 2212) processString += "p";
    if (beamBid == 11)        processString += "e-";
    if (beamBid ==-11)        processString += "e+";
    processString += ">";
    // Set outgoing particles.
    bool foundOutgoing = false;
    for(int i=0; i < int(newp.size()); ++i)
      if ( newp[i].isFinal()
        && ( newp[i].colType() == 0
          || newp[i].idAbs() > 21
          || ( newp[i].id() != 21
            && newp[i].idAbs() > nQuarksMerge) ) ) {
        foundOutgoing = true;
        ostringstream proc;
        proc << "{" << newp[i].name() << "," << newp[i].id()
             << "}";
        processString += proc.str();
      }
    // Set the process string.
    if (foundOutgoing) settingsPtr->word("Merging:Process", processString);
  }

  // Loop through event and count.
  for(int i=0; i < int(newp.size()); ++i)
    if ( newp[i].isFinal()
      && newp[i].colType()!= 0
      && ( newp[i].id() == 21 || newp[i].idAbs() <= nQuarksMerge))
      nPartons++;
  nPartons -= mergingHooksPtr->hardProcess->nQuarksOut();

  // Get number of requested partons.
  string nps_nlo = infoPtr->getEventAttribute("npNLO",true);
  int np_nlo     = (nps_nlo != "") ? atoi((char*)nps_nlo.c_str()) : -1;
  string nps_lo  = infoPtr->getEventAttribute("npLO",true);
  int np_lo      = (nps_lo != "") ? atoi((char*)nps_lo.c_str()) : -1;

  // Set number of requested partons.
  if (np_nlo > -1){
    settingsPtr->mode("Merging:nRequested", np_nlo);
    np_lo = -1;
  } else if (np_lo > -1){
    settingsPtr->mode("Merging:nRequested", np_lo);
    np_nlo = -1;
  } else {
    settingsPtr->mode("Merging:nRequested", nPartons);
    np_nlo = -1;
    np_lo = nPartons;
  }

  mergingHooksPtr->nRequestedSave
    = settingsPtr->mode("Merging:nRequested");

  // Store merging scheme.
  bool isumeps  = mergingHooksPtr->doUMEPSMerging();
  bool isunlops = mergingHooksPtr->doUNLOPSMerging();

  if ( (settingsPtr->flag("Merging:doUNLOPSTree")
     || settingsPtr->flag("Merging:doUNLOPSSubt")
     || settingsPtr->flag("Merging:doUNLOPSSubtNLO")) && np_lo == 0)
     return;

  if (settingsPtr->flag("Merging:doXSectionEstimate")) {
    if (settingsPtr->flag("Merging:doUNLOPSSubt")) {
      settingsPtr->flag("Merging:doUNLOPSTree", true);
      settingsPtr->flag("Merging:doUNLOPSSubt", false);
    }
    if (settingsPtr->flag("Merging:doUNLOPSSubtNLO")) {
      settingsPtr->flag("Merging:doUNLOPSLoop", true);
      settingsPtr->flag("Merging:doUNLOPSSubtNLO", false);
    }
    return;
  }

  // Choose randomly if this event should be treated as subtraction or
  // as regular event. Put the correct settings accordingly.
  double normFactor = 1.;
  if (isunlops && np_nlo == 0 && np_lo == -1) {
    settingsPtr->flag("Merging:doUNLOPSTree", false);
    settingsPtr->flag("Merging:doUNLOPSSubt", false);
    settingsPtr->flag("Merging:doUNLOPSLoop", true);
    settingsPtr->flag("Merging:doUNLOPSSubtNLO", false);
    settingsPtr->mode("Merging:nRecluster",0);
    normFactor *= 1.;
  } else if (isunlops && np_nlo > 0 && np_lo == -1) {
    normFactor *= 2.;
    if (rndmPtr->flat() < 0.5) {
      normFactor *= -1.;
      settingsPtr->flag("Merging:doUNLOPSTree", false);
      settingsPtr->flag("Merging:doUNLOPSSubt", false);
      settingsPtr->flag("Merging:doUNLOPSLoop", false);
      settingsPtr->flag("Merging:doUNLOPSSubtNLO", true);
      settingsPtr->mode("Merging:nRecluster",1);
    } else {
      settingsPtr->flag("Merging:doUNLOPSTree", false);
      settingsPtr->flag("Merging:doUNLOPSSubt", false);
      settingsPtr->flag("Merging:doUNLOPSLoop", true);
      settingsPtr->flag("Merging:doUNLOPSSubtNLO", false);
      settingsPtr->mode("Merging:nRecluster",0);
    }
  } else if (isunlops && np_nlo == -1 && np_lo > -1) {
    normFactor *= 2.;
    if (rndmPtr->flat() < 0.5) {
      normFactor *= -1.;
      //normFactor *=0.;
      settingsPtr->flag("Merging:doUNLOPSTree", false);
      settingsPtr->flag("Merging:doUNLOPSSubt", true);
      settingsPtr->flag("Merging:doUNLOPSLoop", false);
      settingsPtr->flag("Merging:doUNLOPSSubtNLO", false);
      settingsPtr->mode("Merging:nRecluster",1);

      // Double reclustering for exclusive NLO cross sections.
      bool isnlotilde = settingsPtr->flag("Merging:doUNLOPSTilde");
      int nmaxNLO = settingsPtr->mode("Merging:nJetMaxNLO");
      if ( isnlotilde
        && nmaxNLO > 0
        && np_lo <= nmaxNLO+1
        && np_lo > 1 ){
        normFactor *= 2.;
        if (rndmPtr->flat() < 0.5)
          settingsPtr->mode("Merging:nRecluster",2);
      }
    } else {
      settingsPtr->flag("Merging:doUNLOPSTree", true);
      settingsPtr->flag("Merging:doUNLOPSSubt", false);
      settingsPtr->flag("Merging:doUNLOPSLoop", false);
      settingsPtr->flag("Merging:doUNLOPSSubtNLO", false);
      settingsPtr->mode("Merging:nRecluster",0);
    }
  } else if (isumeps && np_lo == 0) {
    settingsPtr->flag("Merging:doUMEPSTree", true);
    settingsPtr->flag("Merging:doUMEPSSubt", false);
    settingsPtr->mode("Merging:nRecluster",0);
  } else if (isumeps && np_lo > 0) {
    normFactor *= 2.;
    if (rndmPtr->flat() < 0.5) {
      normFactor *= -1.;
      settingsPtr->flag("Merging:doUMEPSTree", false);
      settingsPtr->flag("Merging:doUMEPSSubt", true);
      settingsPtr->mode("Merging:nRecluster",1);
    } else {
      settingsPtr->flag("Merging:doUMEPSTree", true);
      settingsPtr->flag("Merging:doUMEPSSubt", false);
      settingsPtr->mode("Merging:nRecluster",0);
    }
  }

  // Remember factor from assigning unitarization procedure.
  unitarizationFactor(normFactor);
  //unitarizationFactor(1.);

}

//--------------------------------------------------------------------------

// Function to steer different merging prescriptions.

int DireMerging::mergeProcess(Event& state){

  // Construct hard process for merging.
  Event process;
  if (doMECs)             process = stripResonances(state);
  else if (doWeakMerging) process = insertResonances(state);
  else                    process = state;

  state = process;

  // Reject input events with problemaatic flavor/color/momentum.
  BeamParticle* beam = ( particleDataPtr->isHadron(beamAPtr->id()) )
    ? beamAPtr : beamBPtr;

  if (!evtUtils->validEvent(process,beam,usePDFmass,false,-1)) {
    string message="Warning in DireMerging::mergeProcess: Input event";
    message+=" has inconsistent flavor/color/momentum. Reject.";
    infoPtr->errorMsg(message);
    return vetoEvent();
  }

  // Clear and reset previous event-by-event information.
  clearInfos();
  initOnEvent(process);
  initSettings();
  int vetoCode = 1;

  // Possibility to apply merging scale to an input event.
  if (!doMECs && applyTMSCut && cutOnProcess(process) ) return vetoEvent();

  // Done if only a cut should be applied.
  if (applyTMSCut && !doMOPS && !doMECs) return 1;

  if (doMerging){

    // Do not include resonance decay products in the counting.
    Event newp( evtUtils->eventForMerging( process, false) );
    int nSteps = mergingHooksPtr->getNumberOfClusteringSteps(newp);
    mergingHooksPtr->nHardNowSave = nSteps;
    if (doMEM) {
      int nFinal(0), nQuarks(0), nGammas(0), nLeptons(0);
      for (int i=3; i < newp.size(); ++i) {
        if (newp[i].idAbs() <   7) nQuarks++;
        if (newp[i].idAbs() == 22) nGammas++;
        if (newp[i].isLepton())    nLeptons++;
        if (newp[i].isFinal())     nFinal++;
      }
      settingsPtr->mode("DireSpace:nFinalMax",nFinal-1);
      settingsPtr->mode("DireTimes:nFinalMax",nFinal-1);
      if (nQuarks > 4) return 1;
      if (nLeptons >4) return 1;
      if (nLeptons+nQuarks>7) return 1;
    }

    // For ME corrections, only do mergingHooksPtr reinitialization here,
    // and do not perform any veto.
    if (doMECs) return 1;

    if (doMEM) mergingHooksPtr->orderHistories(false);

    if (doMOPS) generateUnorderedPoint(process); 

    bool foundHistories = generateHistories(process, false);
    int returnCode = (foundHistories) ? 1 : 0;
    if (doMOPS && myHistory->foundAnyOrderedPaths() && nSteps > 0)
      returnCode = 0;

    // Done if only a cut should be applied.
    if (applyTMSCut && doMOPS) return 1;

    // For interative resummed matrix element method, tag histories and exit.
    if (doMEM) { 
      tagHistories();
      return 1;
    }

    if (doGenerateSubtractions) calculateSubtractions();

    bool useAll = doMOPS;
    double RNpath = getPathIndex(useAll);
    if ((doMOPS && returnCode > 0) || doGenerateMergingWeights)
      returnCode = calculateWeights(RNpath, useAll);

    int tmp_code = getStartingConditions( RNpath, process);
    if (returnCode > 0) returnCode = tmp_code;

    // Ensure that merging weight is not counted twice.
    if (returnCode == 0) vetoEvent();

    if (!allowReject && returnCode < 1) returnCode=1;

    // Store information before leaving.
    if (foundHistories) storeInfos();

    // Overwrite state to return to Pythia.
    state=process;

    if (doMOPS) {
      vetoEvent();
      return returnCode;
    }

    // Veto if we do not want to do event generation.
    if (doExitAfterMerging) return -1;

    return 1;
  }

  // Possibility to perform CKKW-L merging on this event.
  if ( mergingHooksPtr->doCKKWLMerging() )
    vetoCode = mergeProcessCKKWL(process);

  // Possibility to perform UMEPS merging on this event.
  if ( mergingHooksPtr->doUMEPSMerging() )
     vetoCode = mergeProcessUMEPS(process);

  // Possibility to perform UNLOPS merging on this event.
  if ( mergingHooksPtr->doUNLOPSMerging() )
    vetoCode = mergeProcessUNLOPS(process);

  // Overwrite state to return to Pythia.
  state=process;

  return vetoCode;

}

//--------------------------------------------------------------------------

// Function to perform CKKW-L merging on this event.

int DireMerging::mergeProcessCKKWL( Event& process) {

  // Ensure that merging hooks to not veto events in the trial showers.
  mergingHooksPtr->doIgnoreStep(true);
  // For pp > h, allow cut on state, so that underlying processes
  // can be clustered to gg > h
  if ( mergingHooksPtr->getProcessString().compare("pp>h") == 0 )
    mergingHooksPtr->allowCutOnRecState(true);

  // Construct all histories. 
  // This needs to be done because MECs can depend on unordered paths if
  // these unordered paths are ordered up to some point.
  mergingHooksPtr->orderHistories(false);

  // Reset weight of the event.
  double wgt = 1.0;
  mergingHooksPtr->setWeightCKKWL(1.);
  mergingHooksPtr->muMI(-1.);

  // Prepare process record for merging. If Pythia has already decayed
  // resonances used to define the hard process, remove resonance decay
  // products.
  Event newProcess( evtUtils->eventForMerging( process, true) );
  // Store candidates for the splitting V -> qqbar'.
  mergingHooksPtr->storeHardProcessCandidates( newProcess);

  // Calculate number of clustering steps.
  int nSteps = mergingHooksPtr->getNumberOfClusteringSteps(newProcess);
  mergingHooksPtr->nHardNowSave = nSteps;
  double tmsnow = mergingHooksPtr->tmsNow( newProcess );
  int vetoCode = mergingHooksPtr->ktTypeSave;
  mergingHooksPtr->ktTypeSave=-99;

  // Too few steps can be possible if a chain of resonance decays has been
  // removed. In this case, reject this event, since it will be handled in
  // lower-multiplicity samples.
  int nRequested = mergingHooksPtr->nRequested();

  // Store hard event cut information, reset veto information.
  mergingHooksPtr->setHardProcessInfo(nSteps, tmsnow);

  if (nSteps < nRequested && allowReject) return vetoEvent();

  // Reset the minimal tms value, if necessary.
  tmsNowMin     = (nSteps == 0) ? 0. : min(tmsNowMin, tmsnow);

  // Set dummy process scale.
  newProcess.scale(0.0);
  // Generate all histories.
  DireHistory FullHistory( nSteps, 0.0, newProcess, DireClustering(),
    mergingHooksPtr, (*beamAPtr), (*beamBPtr), particleDataPtr, infoPtr,
    trialPartonLevelPtr, fsr, isr, psweights, coupSMPtr, true, true, false, 
    1.0, 1.0, 1.0, 1.0, 0);

  // Project histories onto desired branches, e.g. only ordered paths.
  FullHistory.projectOntoDesiredHistories();

  // Setup to choose shower starting conditions randomly.
  double sumAll(0.), sumFullAll(0.);
  double RN = FullHistory.getPathIndexRandom(rndmPtr, true, sumAll,
     sumFullAll);

  // Do not apply cut if the configuration could not be projected onto an
  // underlying born configuration.
  bool applyCut = nSteps > 0 && FullHistory.select(RN)->nClusterings(NULL) > 0;

  Event core( FullHistory.lowestMultProc(RN) );
  // Set event-specific merging scale cut. Q2-dependent for DIS.
  if ( mergingHooksPtr->getProcessString().compare("e+p>e+j") == 0
    || mergingHooksPtr->getProcessString().compare("e-p>e-j") == 0 ) {

    // Set dynamical merging scale for DIS
    if (FullHistory.isDIS2to2(core)) {
      int iInEl(0), iOutEl(0);
      for ( int i=0; i < core.size(); ++i )
        if ( core[i].idAbs() == 11 ) {
          if ( core[i].status() == -21 ) iInEl  = i;
          if ( core[i].isFinal() )       iOutEl = i;
        }
      double Q      = sqrt( -(core[iInEl].p() - core[iOutEl].p() ).m2Calc());
      double tmsCut = mergingHooksPtr->tmsCut();
      double tmsEvt = tmsCut / sqrt( 1. + pow( tmsCut/ ( 0.5*Q ), 2)  );
      mergingHooksPtr->tms(tmsEvt);

    } else if (FullHistory.isMassless2to2(core)) {
      double mT(1.);
      for ( int i=0; i < core.size(); ++i )
        if ( core[i].isFinal() ) mT *= core[i].mT();
      double Q      = sqrt(mT);
      double tmsCut = mergingHooksPtr->tmsCut();
      double tmsEvt = tmsCut / sqrt( 1. + pow( tmsCut/ ( 0.5*Q ), 2)  );
      mergingHooksPtr->tms(tmsEvt);
    }
  }
  double tmsval = mergingHooksPtr->tms();

  // Enfore merging scale cut if the event did not pass the merging scale
  // criterion.
  bool doVeto = enforceCutOnLHE && applyCut && tmsnow < tmsval && tmsnow > 0.;
  if (tmsnow <= 0. && vetoCode!=-99) {
    if (vetoCode == -1) doVeto = enforceCutOnLHE && applyCut;
    else                doVeto = false;
  }
  //if ( enforceCutOnLHE && applyCut && tmsnow < tmsval ) {
  if (doVeto) {
    string message="Warning in DireMerging::mergeProcessCKKWL: Les Houches Event";
    message+=" fails merging scale cut. Reject event.";
    infoPtr->errorMsg(message);
    return vetoEvent();
  }

  // Check if more steps should be taken.
  int nFinalP(0), nFinalW(0), nFinalZ(0);
  for ( int i = 0; i < core.size(); ++i )
    if ( core[i].isFinal() ) {
      if ( core[i].colType() != 0 ) nFinalP++;
      if ( core[i].idAbs() == 24 )  nFinalW++;
      if ( core[i].idAbs() == 23 )  nFinalZ++;
    }
  bool complete = (FullHistory.select(RN)->nClusterings(NULL) == nSteps) ||
    (nFinalP == 2 && nFinalW+nFinalZ == 0);
  if ( !complete ) {
    string message="Warning in DireMerging::mergeProcessCKKWL: No clusterings";
    message+=" found. History incomplete.";
    infoPtr->errorMsg(message);
  }

  // Calculate CKKWL reweighting for all paths.
  double wgtsum(0.);
  double lastp(0.);
  for ( map<double, DireHistory*>::iterator
    it = FullHistory.branches.begin();
    it != FullHistory.branches.end(); ++it ) {

    // Double to access path.
    double indexNow =  (lastp + 0.5*(it->first - lastp))/sumAll;
    lastp = it->first;

    // Probability of path.
    double probPath = it->second->prodOfProbsFull/sumFullAll;

    FullHistory.select(indexNow)->setSelectedChild();

    // Calculate CKKWL weight:
    double w = FullHistory.weightTREE( trialPartonLevelPtr,
      mergingHooksPtr->AlphaS_FSR(), mergingHooksPtr->AlphaS_ISR(),
      mergingHooksPtr->AlphaEM_FSR(), mergingHooksPtr->AlphaEM_ISR(),
      indexNow);

    wgtsum += probPath*w;
  }
  wgt = wgtsum;

  // Event with production scales set for further (trial) showering
  // and starting conditions for the shower.
  FullHistory.getStartingConditions( RN, process );
  FullHistory.select(RN)->setSelectedChild();

  // If necessary, reattach resonance decay products.
  evtUtils->reattachDecaysAfterMerging(process);

  // Ensure that merging weight is not counted twice.
  bool includeWGT = mergingHooksPtr->includeWGTinXSEC();

  // Save the weight of the event for histogramming.
  if (!includeWGT) mergingHooksPtr->setWeightCKKWL(wgt);

  // Update the event weight.
  if (includeWGT) infoPtr->updateWeight(infoPtr->weight()/CONVERTMB2PB*wgt);

  // Allow merging hooks to veto events from now on.
  mergingHooksPtr->doIgnoreStep(false);

  // If no-emission probability is zero.
  if (allowReject && wgt == 0.) return 0;

  // Done
  return 1;

}

//--------------------------------------------------------------------------

// Function to perform UMEPS merging on this event.

int DireMerging::mergeProcessUMEPS( Event& process) {

  // Initialise which part of UMEPS merging is applied.
  bool doUMEPSTree                = settingsPtr->flag("Merging:doUMEPSTree");
  bool doUMEPSSubt                = settingsPtr->flag("Merging:doUMEPSSubt");
  // Save number of clustering steps
  //mergingHooksPtr->nReclusterSave = settingsPtr->mode("Merging:nRecluster");
  int nReclusterSave = settingsPtr->mode("Merging:nRecluster");
  // Set number of clustering steps to zero, such that cuts and history
  // selection are applied identically to normal and subtractive events.
  mergingHooksPtr->nReclusterSave = 0;
  int nRecluster                  = settingsPtr->mode("Merging:nRecluster");

  // Ensure that merging hooks does not remove emissions.
  mergingHooksPtr->doIgnoreEmissions(true);
  // For pp > h, allow cut on state, so that underlying processes
  // can be clustered to gg > h
  if ( mergingHooksPtr->getProcessString().compare("pp>h") == 0 )
    mergingHooksPtr->allowCutOnRecState(true);
  //// For now, prefer construction of ordered histories.
  //mergingHooksPtr->orderHistories(true);
  // Do not prefer ordered histories.
  mergingHooksPtr->orderHistories(false);

  // Reset weights of the event.
  double wgt   = 1.;
  mergingHooksPtr->setWeightCKKWL(1.);
  mergingHooksPtr->muMI(-1.);

  // Prepare process record for merging. If Pythia has already decayed
  // resonances used to define the hard process, remove resonance decay
  // products.
  Event newProcess( evtUtils->eventForMerging( process, true) );
  // Store candidates for the splitting V -> qqbar'.
  mergingHooksPtr->storeHardProcessCandidates( newProcess );

  // Calculate number of clustering steps.
  int nSteps = mergingHooksPtr->getNumberOfClusteringSteps(newProcess);
  mergingHooksPtr->nHardNowSave = nSteps;

  // Check if event passes the merging scale cut.
  double tmsval   = mergingHooksPtr->tms();
  // Get merging scale in current event.
  double tmsnow  = mergingHooksPtr->tmsNow( newProcess );
  int vetoCode = mergingHooksPtr->ktTypeSave;
  mergingHooksPtr->ktTypeSave=-99;
  int nRequested = mergingHooksPtr->nRequested();

  // Too few steps can be possible if a chain of resonance decays has been
  // removed. In this case, reject this event, since it will be handled in
  // lower-multiplicity samples.
  if (nSteps < nRequested && allowReject) return vetoEvent();

  // Reset the minimal tms value, if necessary.
  tmsNowMin      = (nSteps == 0) ? 0. : min(tmsNowMin, tmsnow);

  // Get random number to choose a path.
  // Set dummy process scale.
  newProcess.scale(0.0);
  // Generate all histories.
  DireHistory FullHistory( nSteps, 0.0, newProcess, DireClustering(),
    mergingHooksPtr, (*beamAPtr), (*beamBPtr), particleDataPtr, infoPtr,
    trialPartonLevelPtr, fsr, isr, psweights, coupSMPtr, true, true, false,
    1.0, 1.0, 1.0, 1.0, 0);
  // Project histories onto desired branches, e.g. only ordered paths.
  FullHistory.projectOntoDesiredHistories();

  // Setup to choose shower starting conditions randomly.
  double sumAll(0.), sumFullAll(0.);
  double RN = FullHistory.getPathIndexRandom(rndmPtr, true, sumAll,
     sumFullAll);

  // Do not apply cut if the configuration could not be projected onto an
  // underlying born configuration.
  bool applyCut = nSteps > 0 && FullHistory.select(RN)->nClusterings(NULL) > 0;

  // Enfore merging scale cut if the event did not pass the merging scale
  // criterion.
  bool doVeto = enforceCutOnLHE && applyCut && tmsnow < tmsval && tmsnow > 0.;
  if (tmsnow <= 0. && vetoCode!=-99) {
    if (vetoCode == -1) doVeto = enforceCutOnLHE && applyCut;
    else                doVeto = false;
  }
  if (doVeto) {
    string message="Warning in DireMerging::mergeProcessUMEPS: Les Houches Event";
    message+=" fails merging scale cut. Reject event.";
    infoPtr->errorMsg(message);
    return vetoEvent();
  }

  // Restore information on nRecluster
  mergingHooksPtr->nReclusterSave = nReclusterSave;
  int nPerformed = 0;

  // Calculate CKKWL reweighting for all paths.
  double wgtsum(0.);
  double lastp(0.);
  for ( map<double, DireHistory*>::iterator
    it = FullHistory.branches.begin();
    it != FullHistory.branches.end(); ++it ) {

    // Double to access path.
    double indexNow =  (lastp + 0.5*(it->first - lastp))/sumAll;
    lastp = it->first;

    // Probability of path.
    double probPath = it->second->prodOfProbsFull/sumFullAll;

    FullHistory.select(indexNow)->setSelectedChild();

    nPerformed = 0;
    // Get the number of clustering steps, to ensure that MPI is properly 
    // initialized. The particular function is *only* used to retrieve nPerformed
    if (doUMEPSSubt) FullHistory.getFirstClusteredEventAboveTMS( 
      indexNow, nRecluster, newProcess, nPerformed, false );
    mergingHooksPtr->nMinMPI(nSteps - nPerformed);

    // Calculate CKKWL weight:
    double w = doUMEPSTree
     //? FullHistory.weight_UMEPS_TREE( trialPartonLevelPtr,
      ? FullHistory.weightTREE( trialPartonLevelPtr,
          mergingHooksPtr->AlphaS_FSR(), mergingHooksPtr->AlphaS_ISR(),
          mergingHooksPtr->AlphaEM_FSR(), mergingHooksPtr->AlphaEM_ISR(),
          indexNow)
      : FullHistory.weight_UMEPS_SUBT( trialPartonLevelPtr,
          mergingHooksPtr->AlphaS_FSR(), mergingHooksPtr->AlphaS_ISR(),
          mergingHooksPtr->AlphaEM_FSR(), mergingHooksPtr->AlphaEM_ISR(),
          indexNow);
    wgtsum += probPath*w;
  }
  wgt = wgtsum;

  // Event with production scales set for further (trial) showering
  // and starting conditions for the shower.
  if ( doUMEPSTree ) FullHistory.getStartingConditions( RN, process );
  // Do reclustering steps.
  else FullHistory.getFirstClusteredEventAboveTMS( RN, nRecluster, process,
    nPerformed, true );

  // Ensure that merging weight is not counted twice.
  bool includeWGT = mergingHooksPtr->includeWGTinXSEC();

  // Save the weight of the event for histogramming.
  if (!includeWGT) mergingHooksPtr->setWeightCKKWL(unitarizationFactor()*wgt);

  // Update the event weight.
  if (includeWGT) infoPtr->updateWeight(infoPtr->weight()/CONVERTMB2PB*
                                        unitarizationFactor()*wgt);

  // Set QCD 2->2 starting scale different from arbitrary scale in LHEF!
  // --> Set to minimal mT of partons.
  int nFinal = 0;
  double muf = process[0].e();
  for ( int i=0; i < process.size(); ++i )
  if ( process[i].isFinal()
    && (process[i].colType() != 0 || process[i].id() == 22 ) ) {
    nFinal++;
    muf = min( muf, abs(process[i].mT()) );
  }

  // For pure QCD dijet events (only!), set the process scale to the
  // transverse momentum of the outgoing partons.
  // Calculate number of clustering steps.
  int nStepsNew = mergingHooksPtr->getNumberOfClusteringSteps(process);
  if ( nStepsNew == 0
    && ( mergingHooksPtr->getProcessString().compare("pp>jj") == 0
      || mergingHooksPtr->getProcessString().compare("pp>aj") == 0) )
    process.scale(muf);

  // Reset hard process candidates (changed after clustering a parton).
  mergingHooksPtr->storeHardProcessCandidates( process );

  // Check if resonance structure has been changed
  //  (e.g. because of clustering W/Z/gluino)
  vector <int> oldResonance;
  for ( int i=0; i < newProcess.size(); ++i )
    if ( newProcess[i].status() == 22 )
      oldResonance.push_back(newProcess[i].id());
  vector <int> newResonance;
  for ( int i=0; i < process.size(); ++i )
    if ( process[i].status() == 22 )
      newResonance.push_back(process[i].id());
  // Compare old and new resonances
  for ( int i=0; i < int(oldResonance.size()); ++i )
    for ( int j=0; j < int(newResonance.size()); ++j )
      if ( newResonance[j] == oldResonance[i] ) {
        oldResonance[i] = 99;
        break;
      }
  bool hasNewResonances = (newResonance.size() != oldResonance.size());
  for ( int i=0; i < int(oldResonance.size()); ++i )
    hasNewResonances = (oldResonance[i] != 99);

  // If necessary, reattach resonance decay products.
  if (!hasNewResonances) evtUtils->reattachDecaysAfterMerging(process);

  // Allow merging hooks to remove emissions from now on.
  mergingHooksPtr->doIgnoreEmissions(false);

  // If no-emission probability is zero.
  if (allowReject && wgt == 0.) return 0;

  // If the resonance structure of the process has changed due to reclustering,
  // redo the resonance decays in Pythia::next()
  if (hasNewResonances) return 2;

  // Done
  return 1;

}

//--------------------------------------------------------------------------

// Function to perform UNLOPS merging on this event.

int DireMerging::mergeProcessUNLOPS( Event& process) {

  bool useAll = true;

  // Initialise which part of UNLOPS merging is applied.
  //bool nloTilde         = settingsPtr->flag("Merging:doUNLOPSTilde");
  bool doUNLOPSTree     = settingsPtr->flag("Merging:doUNLOPSTree");
  bool doUNLOPSLoop     = settingsPtr->flag("Merging:doUNLOPSLoop");
  bool doUNLOPSSubt     = settingsPtr->flag("Merging:doUNLOPSSubt");
  bool doUNLOPSSubtNLO  = settingsPtr->flag("Merging:doUNLOPSSubtNLO");
  // Save number of clustering steps
  int nReclusterSave = settingsPtr->mode("Merging:nRecluster");
  int nRecluster = nReclusterSave;
  mergingHooksPtr->nReclusterSave = nReclusterSave;
  if (doUNLOPSSubt || doUNLOPSSubtNLO) mergingHooksPtr->nReclusterSave = 0;

  // Ensure that merging hooks to not remove emissions
  mergingHooksPtr->doIgnoreEmissions(true);
  //// For now, prefer construction of ordered histories.
  //mergingHooksPtr->orderHistories(true);
  //if (doUNLOPSLoop || doUNLOPSSubtNLO) mergingHooksPtr->orderHistories(false);
  // Do not prefer ordered histories.
  mergingHooksPtr->orderHistories(false);
  // For pp > h, allow cut on state, so that underlying processes
  // can be clustered to gg > h
  if ( mergingHooksPtr->getProcessString().compare("pp>h") == 0)
    mergingHooksPtr->allowCutOnRecState(true);

  // Reset weight of the event.
  double wgt      = 1.;
  mergingHooksPtr->setWeightCKKWL(1.);
  // Reset the O(alphaS)-term of the UMEPS weight.
  double wgtFIRST = 0.;
  mergingHooksPtr->setWeightFIRST(0.);
  mergingHooksPtr->muMI(-1.);

  double tevol(-1.), zevol(-1.), phi(-1.);
  ostringstream sstream;
  sstream << scientific << setprecision(8);
  sstream.str(""); sstream << tevol;
  infoPtr->setEventAttribute("t",   sstream.str());
  infoPtr->setEventAttribute("t1",   sstream.str());
  infoPtr->setEventAttribute("t2",   sstream.str());
  infoPtr->setEventAttribute("t3",   sstream.str());
  sstream.str(""); sstream << zevol;
  infoPtr->setEventAttribute("z",   sstream.str());
  infoPtr->setEventAttribute("z1",   sstream.str());
  infoPtr->setEventAttribute("z2",   sstream.str());
  infoPtr->setEventAttribute("z3",   sstream.str());
  sstream.str(""); sstream << phi;
  infoPtr->setEventAttribute("phi", sstream.str());
  infoPtr->setEventAttribute("phi1", sstream.str());
  infoPtr->setEventAttribute("phi2", sstream.str());
  infoPtr->setEventAttribute("phi3", sstream.str());
  sstream.str(""); sstream << 0.;
  infoPtr->setEventAttribute("unlops_weight", sstream.str());

  // Prepare process record for merging. If Pythia has already decayed
  // resonances used to define the hard process, remove resonance decay
  // products.
  Event newProcess( evtUtils->eventForMerging( process, true) );
  // Store candidates for the splitting V -> qqbar'
  mergingHooksPtr->storeHardProcessCandidates( newProcess );

  // Calculate number of clustering steps
  int nSteps = mergingHooksPtr->getNumberOfClusteringSteps(newProcess);
  mergingHooksPtr->nHardNowSave = nSteps;

  // Potentially recluster real emission jets for powheg input containing
  // "too many" jets, i.e. real-emission kinematics.
  int nRequested = mergingHooksPtr->nRequested();
  bool containsRealKin = nSteps > nRequested && nSteps > 0;
  // Too few steps can be possible if a chain of resonance decays has been
  // removed. In this case, reject this event, since it will be handled in
  // lower-multiplicity samples.
  if (nSteps < nRequested) {
    string message="Warning in DireMerging::mergeProcessUNLOPS: Input event";
    message+=" does not contain enough partons after removing decay products.";
    infoPtr->errorMsg(message);
    return vetoEvent();
  }

  // Check if event passes the merging scale cut.
  double tmsval  = mergingHooksPtr->tms();
  // Get merging scale in current event.
  double tmsnow  = !containsRealKin ? mergingHooksPtr->tmsNow(newProcess) : 0.;
  int vetoCode = mergingHooksPtr->ktTypeSave;
  mergingHooksPtr->ktTypeSave=-99;

  // Reset the minimal tms value, if necessary.
  tmsNowMin = (nSteps == 0) ? 0. : min(tmsNowMin, tmsnow);

  // Set dummy process scale.
  newProcess.scale(0.0);
  // Generate all histories
  DireHistory FullHistory( nSteps, 0.0, newProcess, DireClustering(),
     mergingHooksPtr, (*beamAPtr), (*beamBPtr), particleDataPtr, infoPtr,
     trialPartonLevelPtr, fsr, isr, psweights, coupSMPtr, true, true,
     containsRealKin, 1.0, 1.0, 1.0, 1.0, 0);

  // If need be, perform one clustering of real-emission state to one
  // underlying Born, with uniform probability
  DireHistory* picked = &FullHistory;
  if ( containsRealKin && !FullHistory.children.empty()) {
    //FullHistory.projectOntoDesiredHistories();
    //picked = FullHistory.getLeafByPathProb(rndmPtr);
    //picked->rescaleProbs();
    picked = FullHistory.getLeafDemocratic(rndmPtr);
    picked->rescaleProbs();
    //picked = FullHistory.getLeafByClusterProb(rndmPtr);
    picked->mother = 0;
    //mergingHooksPtr->orderHistories(true);
    picked->projectOntoDesiredHistories();
  } else {
    // Project histories onto desired branches, e.g. only ordered paths.
    //if (doUNLOPSLoop || doUNLOPSSubtNLO) mergingHooksPtr->orderHistories(true);
    //mergingHooksPtr->orderHistories(true);
    picked->projectOntoDesiredHistories();
  }

  // Pick path at random (assuming that the weight of ALL paths are used
  // later to perform reweighting.
  double sumAll(0.), sumFullAll(0.);
  double RN = picked->getPathIndexRandom(rndmPtr, true, sumAll,
    sumFullAll);

  // Pick path according to probability: Overwrite random number! 
  if (!useAll) RN = rndmPtr->flat();

  // Now enfore merging scale cut if the input event did not pass the merging
  // scale. Note: Do not apply cut if the configuration could not be projected
  // onto an underlying born configuration.
  bool doVetoInputState = enforceCutOnLHE && !containsRealKin
    && nSteps > 0
    && FullHistory.select(RN)->nClusterings(NULL) > 0
    && nSteps == nRequested
    && tmsnow < tmsval;
  // If merging scale is not well-defined, check veto code instead!
  if (tmsnow <= 0. && vetoCode!=-99) {
    if (vetoCode == -1) doVetoInputState = enforceCutOnLHE && !containsRealKin
      && nSteps > 0
      && FullHistory.select(RN)->nClusterings(NULL) > 0
      && nSteps == nRequested;
    else                doVetoInputState = false;
  }
  if (doVetoInputState) {
    string message="Warning in DireMerging::mergeProcessUNLOPS: Input event";
    message+=" fails merging scale cut. Reject event.";
    infoPtr->errorMsg(message);
    return vetoEvent();
  }

  // Remove real emission events without underlying Born configuration from
  // the loop sample, since such states will be taken care of by tree-level
  // samples.
  if ( containsRealKin && !allowIncompleteReal
    //&& FullHistory.select(RN)->nClusterings() == 0 ) {
    //&& nSteps > 1 && picked->select(RN)->nClusterings(NULL) == 0 ) {
    && nSteps > 1 && picked == &FullHistory) {
    string message="Warning in DireMerging::mergeProcessUNLOPS: Input";
    message+=" real-correction event has no underlying Born. Reject.";
    infoPtr->errorMsg(message);
    return vetoEvent();
  }

  // Perform one reclustering for real emission kinematics, then apply
  // merging scale cut on underlying Born kinematics.
  if ( containsRealKin && enforceCutOnLHE) {
    //int nReclusterSave = mergingHooksPtr->nReclusterSave;
    mergingHooksPtr->nReclusterSave = 1;
    double tnowNew  = mergingHooksPtr->tmsNow( picked->state);
    bool doVetoClusteredState = enforceCutOnLHE
      && nSteps > 0 && nRequested > 0
      && tnowNew < tmsval;
    vetoCode = mergingHooksPtr->ktTypeSave;
    mergingHooksPtr->ktTypeSave=-99;
    if (vetoCode==-99) doVetoClusteredState = false;
    if (tnowNew <= 0. && vetoCode!=-99) {
      if (vetoCode == -1) doVetoClusteredState = enforceCutOnLHE
        && nSteps > 0 && nRequested > 0;
      else                doVetoClusteredState = false;
    }
    mergingHooksPtr->nReclusterSave = nReclusterSave;
    // Veto if underlying Born kinematics do not pass merging scale cut.
    if (doVetoClusteredState) {
      string message="Warning in DireMerging::mergeProcessUNLOPS: Clustered";
      message+=" input event fails merging scale cut. Reject event.";
      infoPtr->errorMsg(message);
      return vetoEvent();
    }
  }

  mergingHooksPtr->nReclusterSave
    = nReclusterSave;
  mergingHooksPtr->nHardNowSave
    = mergingHooksPtr->getNumberOfClusteringSteps(picked->state);

  // New UNLOPS strategy based on UN2LOPS.
  bool doUNLOPS2 = false;
  int depth = (!doUNLOPS2) ? -1 : ( (containsRealKin) ? nSteps-1 : nSteps);

  // Calculate reweighting, considering all paths.
  if (useAll) {
    double wgtsum(0.);
    double lastp(0.);
    for ( map<double, DireHistory*>::iterator
      it = picked->branches.begin();
      it != picked->branches.end(); ++it ) {

      // Double to access path.
      double indexNow =  (lastp + 0.5*(it->first - lastp))/sumAll;
      lastp = it->first;

      // Probability of path.
      double probPath = it->second->prodOfProbsFull/sumFullAll;

      picked->select(indexNow)->setSelectedChild();
      mergingHooksPtr->nMinMPI(
        mergingHooksPtr->getNumberOfClusteringSteps(picked->state));

      // Calculate weights.
      double w(0.);
      // Do LO or first part of NLO tree-level reweighting
      if( doUNLOPSTree ) {
        // Perform reweighting with Sudakov factors, save as ratios and
        // PDF ratio weights
        w = picked->weight_UNLOPS_TREE( trialPartonLevelPtr,
                mergingHooksPtr->AlphaS_FSR(), mergingHooksPtr->AlphaS_ISR(),
                mergingHooksPtr->AlphaEM_FSR(), mergingHooksPtr->AlphaEM_ISR(),
                indexNow, depth);
      } else if( doUNLOPSLoop ) {
        // Set event scales properly, reweight for new UNLOPS
        w = picked->weight_UNLOPS_LOOP( trialPartonLevelPtr,
                mergingHooksPtr->AlphaS_FSR(), mergingHooksPtr->AlphaS_ISR(),
                mergingHooksPtr->AlphaEM_FSR(), mergingHooksPtr->AlphaEM_ISR(),
                indexNow, depth);
      } else if( doUNLOPSSubtNLO ) {
        // Set event scales properly, reweight for new UNLOPS
        w = picked->weight_UNLOPS_SUBTNLO( trialPartonLevelPtr,
                mergingHooksPtr->AlphaS_FSR(), mergingHooksPtr->AlphaS_ISR(),
                mergingHooksPtr->AlphaEM_FSR(), mergingHooksPtr->AlphaEM_ISR(),
                indexNow, depth);
      } else if( doUNLOPSSubt ) {
        // Perform reweighting with Sudakov factors, save as ratios and
        // PDF ratio weights
        w = picked->weight_UNLOPS_SUBT( trialPartonLevelPtr,
                mergingHooksPtr->AlphaS_FSR(), mergingHooksPtr->AlphaS_ISR(),
                mergingHooksPtr->AlphaEM_FSR(), mergingHooksPtr->AlphaEM_ISR(),
                indexNow, depth);
      }
      wgtsum += probPath*w;
    }
    wgt = wgtsum;

  // Use only one path for weight.
  } else {

    // Probability of path, including sign!
    double probPath = (picked->select(RN)->prodOfProbsFull/sumFullAll)
                    / (picked->select(RN)->prodOfProbs/sumAll);

    picked->select(RN)->setSelectedChild();
    mergingHooksPtr->nMinMPI(
      mergingHooksPtr->getNumberOfClusteringSteps(picked->state));

    // Calculate weights.
    double w(0.);
    // Do LO or first part of NLO tree-level reweighting
    if( doUNLOPSTree ) {
      // Perform reweighting with Sudakov factors, save as ratios and
      // PDF ratio weights
      w = picked->weight_UNLOPS_TREE( trialPartonLevelPtr,
              mergingHooksPtr->AlphaS_FSR(), mergingHooksPtr->AlphaS_ISR(),
              mergingHooksPtr->AlphaEM_FSR(), mergingHooksPtr->AlphaEM_ISR(),
              RN, depth);
    } else if( doUNLOPSLoop ) {
      // Set event scales properly, reweight for new UNLOPS
      w = picked->weight_UNLOPS_LOOP( trialPartonLevelPtr,
              mergingHooksPtr->AlphaS_FSR(), mergingHooksPtr->AlphaS_ISR(),
              mergingHooksPtr->AlphaEM_FSR(), mergingHooksPtr->AlphaEM_ISR(),
              RN, depth);
    } else if( doUNLOPSSubtNLO ) {
      // Set event scales properly, reweight for new UNLOPS
      w = picked->weight_UNLOPS_SUBTNLO( trialPartonLevelPtr,
              mergingHooksPtr->AlphaS_FSR(), mergingHooksPtr->AlphaS_ISR(),
              mergingHooksPtr->AlphaEM_FSR(), mergingHooksPtr->AlphaEM_ISR(),
              RN, depth);
    } else if( doUNLOPSSubt ) {
      // Perform reweighting with Sudakov factors, save as ratios and
      // PDF ratio weights
      w = picked->weight_UNLOPS_SUBT( trialPartonLevelPtr,
              mergingHooksPtr->AlphaS_FSR(), mergingHooksPtr->AlphaS_ISR(),
              mergingHooksPtr->AlphaEM_FSR(), mergingHooksPtr->AlphaEM_ISR(),
              RN, depth);
    }
    wgt = probPath*w;

  }

  // For tree-level or subtractive sammples, rescale with k-Factor
  if ( doUNLOPSTree || doUNLOPSSubt ){
    // Find k-factor
    double kFactor = 1.;
    if ( nSteps > mergingHooksPtr->nMaxJetsNLO() )
      kFactor = mergingHooksPtr->kFactor( mergingHooksPtr->nMaxJetsNLO() );
    else kFactor = mergingHooksPtr->kFactor(nSteps);
    // For NLO merging, rescale CKKW-L weight with k-factor
    wgt *= kFactor;
  }

  // Ensure that merging weight is not counted twice.
  bool includeWGT = mergingHooksPtr->includeWGTinXSEC();

  // Save the weight of the event for histogramming.
  if (!includeWGT) mergingHooksPtr->setWeightCKKWL(unitarizationFactor()*wgt);

  // Check if we need to subtract the O(\alpha_s)-term. If the number
  // of additional partons is larger than the number of jets for
  // which loop matrix elements are available, do standard UMEPS.
  int nMaxNLO     = mergingHooksPtr->nMaxJetsNLO();
  bool doOASTree  = doUNLOPSTree && nSteps <= nMaxNLO;
  bool doOASSubt  = doUNLOPSSubt && nSteps <= nMaxNLO+1 && nSteps > 0;

  // Now begin NLO part for tree-level events
  if ( doOASTree || doOASSubt ) {

    // Decide on which order to expand to.
    int order = ( nSteps > 0 && nSteps <= nMaxNLO) ? 1 : -1;

    // Calculate reweighting, considering all paths.
    if (useAll) {

      // Calculate reweighting, considering all paths.
      double wgtsum(0.), lastp(0.);
      for ( map<double, DireHistory*>::iterator
        it = picked->branches.begin();
        it != picked->branches.end(); ++it ) {
        // Double to access path.
        double indexNow =  (lastp + 0.5*(it->first - lastp))/sumAll;
        lastp = it->first;
        // Probability of path.
        double probPath = it->second->prodOfProbsFull/sumFullAll;
        picked->select(indexNow)->setSelectedChild();
        mergingHooksPtr->nMinMPI(
          mergingHooksPtr->getNumberOfClusteringSteps(picked->state));

        // Calculate terms in expansion of the CKKW-L weight.
        double w = picked->weight_UNLOPS_CORRECTION( order,
          trialPartonLevelPtr, mergingHooksPtr->AlphaS_FSR(),
          mergingHooksPtr->AlphaS_ISR(), mergingHooksPtr->AlphaEM_FSR(),
          mergingHooksPtr->AlphaEM_ISR(), indexNow, rndmPtr );
        wgtsum += probPath*w;
      }
      wgtFIRST = wgtsum;

    // Use only one path for weight.
    } else {

      // Probability of path.
      double probPath = (picked->select(RN)->prodOfProbsFull/sumFullAll)
                      / (picked->select(RN)->prodOfProbs/sumAll);
      picked->select(RN)->setSelectedChild();
      mergingHooksPtr->nMinMPI(
        mergingHooksPtr->getNumberOfClusteringSteps(picked->state));

      // Calculate terms in expansion of the CKKW-L weight.
      double w = picked->weight_UNLOPS_CORRECTION( order,
        trialPartonLevelPtr, mergingHooksPtr->AlphaS_FSR(),
        mergingHooksPtr->AlphaS_ISR(), mergingHooksPtr->AlphaEM_FSR(),
        mergingHooksPtr->AlphaEM_ISR(), RN, rndmPtr );
      wgtFIRST = probPath*w;

    }

    // Set the subtractive weight to the value calculated so far
    if (!includeWGT)
      mergingHooksPtr->setWeightFIRST(unitarizationFactor()*wgtFIRST);
    else
      mergingHooksPtr->setWeightFIRST(0.);

    // Subtract the O(\alpha_s)-term from the CKKW-L weight
    // If PDF contributions have not been included, subtract these later
    // New UNLOPS based on UN2LOPS.
    if (doUNLOPS2 && order > -1) wgt = -wgt*(wgtFIRST-1.);
    else if (order > -1) wgt = wgt - wgtFIRST;

  }

  // Event with production scales set for further (trial) showering
  // and starting conditions for the shower.
  int nPerformed = 0;
  if (!doUNLOPSSubt && !doUNLOPSSubtNLO) picked->getStartingConditions(RN,
    process);
  // Do reclustering steps.
  else picked->getFirstClusteredEventAboveTMS(RN, nRecluster, process,
    nPerformed, true );

  DireHistory* underlyingNode = picked->getLeafByPathProb(RN);
  if ( underlyingNode != picked
    && mergingHooksPtr->getNumberOfClusteringSteps(process) == 1) {
    tevol = underlyingNode->clusterIn.pT();
    zevol = underlyingNode->clusterIn.z();
    phi   = underlyingNode->clusterIn.phi();

  }
  sstream.str(""); sstream << tevol;
  infoPtr->setEventAttribute("t",   sstream.str());
  sstream.str(""); sstream << zevol;
  infoPtr->setEventAttribute("z",   sstream.str());
  sstream.str(""); sstream << phi;
  infoPtr->setEventAttribute("phi", sstream.str());

  picked->select(RN)->setClusteringInfos(
    mergingHooksPtr->getNumberOfClusteringSteps(process));

  // Set QCD 2->2 starting scale different from arbitrary scale in LHEF!
  // --> Set to minimal mT of partons.
  int nFinal = 0;
  double muf = process[0].e();
  for ( int i=0; i < process.size(); ++i )
  if ( process[i].isFinal()
    && (process[i].colType() != 0 || process[i].id() == 22 ) ) {
    nFinal++;
    muf = min( muf, abs(process[i].mT()) );
  }
  // For pure QCD dijet events (only!), set the process scale to the
  // transverse momentum of the outgoing partons.
  if ( nSteps == 0 && nFinal == 2
    && ( mergingHooksPtr->getProcessString().compare("pp>jj") == 0
      || mergingHooksPtr->getProcessString().compare("pp>aj") == 0) )
    process.scale(muf);

  // Reset hard process candidates (changed after clustering a parton).
  mergingHooksPtr->storeHardProcessCandidates( process );

  // Check if resonance structure has been changed
  //  (e.g. because of clustering W/Z/gluino)
  vector <int> oldResonance;
  for ( int i=0; i < newProcess.size(); ++i )
    if ( newProcess[i].status() == 22 )
      oldResonance.push_back(newProcess[i].id());
  vector <int> newResonance;
  for ( int i=0; i < process.size(); ++i )
    if ( process[i].status() == 22 )
      newResonance.push_back(process[i].id());
  // Compare old and new resonances
  for ( int i=0; i < int(oldResonance.size()); ++i )
    for ( int j=0; j < int(newResonance.size()); ++j )
      if ( newResonance[j] == oldResonance[i] ) {
        oldResonance[i] = 99;
        break;
      }
  bool hasNewResonances = (newResonance.size() != oldResonance.size());
  for ( int i=0; i < int(oldResonance.size()); ++i )
    hasNewResonances = (oldResonance[i] != 99);

  // If necessary, reattach resonance decay products.
  if (!hasNewResonances) evtUtils->reattachDecaysAfterMerging(process);

  // Update the event weight.
  if (includeWGT) infoPtr->updateWeight(infoPtr->weight()/CONVERTMB2PB
                                       *unitarizationFactor()*wgt);

  // Allow merging hooks to remove emissions from now on.
  mergingHooksPtr->doIgnoreEmissions(false);

  // If no-emission probability is zero.
  if (allowReject && wgt == 0.) return 0;

  // If the resonance structure of the process has changed due to reclustering,
  // redo the resonance decays in Pythia::next()
  if (hasNewResonances) return 2;

  // Done
  return 1;

}

//--------------------------------------------------------------------------

// Function to set up all histories for an event.

bool DireMerging::generateHistories( const Event& process, bool orderedOnly) {

  // Input not valid.
  if (!validEvent(process)) {
    cout << "Warning in DireMerging::generateHistories: Input event "
         << "has invalid flavour or momentum structure, thus reject. " << endl;
    return false;
  }

  // Clear previous history.
  if (myHistory) delete myHistory;

  // For now, prefer construction of ordered histories.
  mergingHooksPtr->orderHistories(orderedOnly);

//?????????????? comment back in!!
  if (doMOPS) mergingHooksPtr->orderHistories(false);

  // For pp > h, allow cut on state, so that underlying processes
  // can be clustered to gg > h
  if ( mergingHooksPtr->getProcessString().compare("pp>h") == 0)
    mergingHooksPtr->allowCutOnRecState(true);

  // Prepare process record for merging. If Pythia has already decayed
  // resonances used to define the hard process, remove resonance decay
  // products.
  Event newProcess( evtUtils->eventForMerging( process, true) );
  // Store candidates for the splitting V -> qqbar'
  mergingHooksPtr->storeHardProcessCandidates( newProcess );

  // Calculate number of clustering steps
  int nSteps = mergingHooksPtr->getNumberOfClusteringSteps(newProcess);

nSteps++;
  mergingHooksPtr->nHardNowSave = nSteps;

  // Set dummy process scale.
  newProcess.scale(0.0);
  // Generate all histories
  myHistory = new DireHistory( nSteps, 0.0, newProcess, DireClustering(),
        mergingHooksPtr, (*beamAPtr), (*beamBPtr), particleDataPtr, infoPtr,
        trialPartonLevelPtr, fsr, isr, psweights, coupSMPtr, true, true, false,
        1.0, 1.0, 1.0, 1.0, 0);
  // Project histories onto desired branches, e.g. only ordered paths.
  bool foundHistories = myHistory->projectOntoDesiredHistories();

  // Done
  return (doMOPS ? foundHistories : true);

}

//--------------------------------------------------------------------------

void DireMerging::tagHistories() {

  // Nothing to do if no histories were found.
  if (!myHistory || myHistory->branches.empty()) return;
  
  // Tag history paths as "signal" or "background"
  for ( map<double, DireHistory*>::iterator it = myHistory->branches.begin();
    it != myHistory->branches.end(); ++it )
    it->second->tagPath(it->second);

  double sumAll(0.), sumFullAll(0.), lastp(0.);
  for ( map<double, DireHistory*>::iterator it = myHistory->branches.begin();
    it != myHistory->branches.end(); ++it ) {
    sumAll     += it->second->prodOfProbs;
    sumFullAll += it->second->prodOfProbsFull;
  }

  // Sum up signal and background probabilities.
  vector<double> sumSignalProb(createvector<double>(0.)(0.)(0.)),
    sumBkgrndProb(createvector<double>(0.)(0.)(0.));

  for ( map<double, DireHistory*>::iterator it = myHistory->branches.begin();
    it != myHistory->branches.end(); ++it ) {

    if (it->second == myHistory) continue;

    // Get ME weight.
    double prob = it->second->prodOfProbsFull;
    // Reweight with Sudakovs, couplings and PDFs.
    double indexNow =  (lastp + 0.5*(it->first - lastp))/sumAll;
    lastp = it->first;
    myHistory->select(indexNow)->setSelectedChild();
    vector<double> w = myHistory->weightMEM( trialPartonLevelPtr,
      mergingHooksPtr->AlphaS_FSR(), mergingHooksPtr->AlphaEM_FSR(), indexNow);
    for (unsigned int i=0; i < w.size(); ++i) {
      //if (it->second->tag() == 0) sumSignalProb[i] += prob*w[i];
      //if (it->second->hasTag("higgs") ) sumSignalProb[i] += prob*w[i];
      //else                              sumBkgrndProb[i] += prob*w[i];
      totalProbSave[i] += prob*w[i];
      if (it->second->hasTag("higgs") ) signalProbSave["higgs"][i] += prob*w[i];
      else                              bkgrndProbSave["higgs"][i] += prob*w[i];
      if (it->second->hasTag("qed") )   signalProbSave["qed"][i]   += prob*w[i];
      else                              bkgrndProbSave["qed"][i]   += prob*w[i];
      if (it->second->hasTag("qcd") )   signalProbSave["qcd"][i]   += prob*w[i];
      else                              bkgrndProbSave["qcd"][i]   += prob*w[i];

      if (it->second->hasTag("higgs") ) signalProbSave["higgs-subt"][i] += prob*(w[i]-1.);
      else                              bkgrndProbSave["higgs-subt"][i] += prob*(w[i]-1.);
      if (it->second->hasTag("higgs") ) signalProbSave["higgs-nosud"][i] += prob;
      else                              bkgrndProbSave["higgs-nosud"][i] += prob;

      if (it->second->hasTag("bsm") )   signalProbSave["bsm"][i] += prob*w[i];
      else                              bkgrndProbSave["bsm"][i] += prob*w[i];
    }
  }

}

//--------------------------------------------------------------------------

double DireMerging::getPathIndex( bool useAll) {
  if (!useAll) return rndmPtr->flat();
  double sumAll(0.), sumFullAll(0.);
  return myHistory->getPathIndexRandom(rndmPtr, useAll,sumAll, sumFullAll);
}

//--------------------------------------------------------------------------

// Function to set up all histories for an event.

bool DireMerging::calculateSubtractions() {

  // Store shower subtractions.
  clearSubtractions();
  for ( int i = 0 ; i < int(myHistory->children.size()); ++i) {

    // Need to reattach resonance decays, if necessary.
    Event psppoint = myHistory->children[i]->state;
    // Reset hard process candidates (changed after clustering a parton).
    mergingHooksPtr->storeHardProcessCandidates( psppoint );

    // Check if resonance structure has been changed
    //  (e.g. because of clustering W/Z/gluino)
    vector <int> oldResonance;
    for ( int n=0; n < myHistory->state.size(); ++n )
      if ( myHistory->state[n].status() == 22 )
        oldResonance.push_back(myHistory->state[n].id());
    vector <int> newResonance;
    for ( int n=0; n < psppoint.size(); ++n )
      if ( psppoint[n].status() == 22 )
        newResonance.push_back(psppoint[n].id());
    // Compare old and new resonances
    for ( int n=0; n < int(oldResonance.size()); ++n )
      for ( int m=0; m < int(newResonance.size()); ++m )
        if ( newResonance[m] == oldResonance[n] ) {
          oldResonance[n] = 99;
          break;
        }
    bool hasNewResonances = (newResonance.size() != oldResonance.size());
    for ( int n=0; n < int(oldResonance.size()); ++n )
      hasNewResonances = (oldResonance[n] != 99);

    // If necessary, reattach resonance decay products.
    if (!hasNewResonances) evtUtils->reattachDecaysAfterMerging(psppoint);
    else {
      cout << "Warning in DireMerging::generateHistories: Resonance "
           << "structure changed due to clustering. Cannot attach decay "
           << "products correctly." << endl;
    }

    double prob = myHistory->children[i]->clusterProb;

    // Switch from 4pi to 8pi convention
    prob *= 2.;

    // Get clustering variables.
    int rad = myHistory->children[i]->clusterIn.radPos;
    int emt = myHistory->children[i]->clusterIn.emt1Pos;
    int rec = myHistory->children[i]->clusterIn.recPos;
    double z = kinRels->zEvol(myHistory->state[rad],
      myHistory->state[emt],
      myHistory->state[rec]);
    double t = kinRels->tEvol(myHistory->state[rad],
      myHistory->state[emt],
      myHistory->state[rec]);
    double m2dip = abs(-2.*myHistory->state[emt].p()*myHistory->state[rad].p()
                      -2.*myHistory->state[emt].p()*myHistory->state[rec].p()
                       +2.*myHistory->state[rad].p()*myHistory->state[rec].p());
    double kappa2 = t/m2dip;
    double xCS        = (z*(1-z) - kappa2) / (1 -z);

    // For II dipoles, scale with 1/xCS.
    prob *= 1./xCS;

    // Multiply with ME correction.
    prob *= myHistory->MECnum/myHistory->MECden;

    // Attach point to list of shower subtractions.
    appendSubtraction( prob, psppoint);

  }

  // Restore stored hard process candidates
  mergingHooksPtr->storeHardProcessCandidates(  myHistory->state );

  // Done
  return true;

}

//--------------------------------------------------------------------------

// Function to calulate the weights used for UNLOPS merging.

int DireMerging::calculateWeights( double RNpath, bool useAll ) {

  // Initialise which part of UNLOPS merging is applied.
  bool nloTilde         = settingsPtr->flag("Merging:doUNLOPSTilde");
  bool doUNLOPSTree     = settingsPtr->flag("Merging:doUNLOPSTree");
  bool doUNLOPSLoop     = settingsPtr->flag("Merging:doUNLOPSLoop");
  bool doUNLOPSSubt     = settingsPtr->flag("Merging:doUNLOPSSubt");
  bool doUNLOPSSubtNLO  = settingsPtr->flag("Merging:doUNLOPSSubtNLO");
  // Save number of looping steps
  mergingHooksPtr->nReclusterSave = settingsPtr->mode("Merging:nRecluster");
  int nRecluster        = settingsPtr->mode("Merging:nRecluster");

  // Ensure that merging hooks to not remove emissions
  mergingHooksPtr->doIgnoreEmissions(true);
  mergingHooksPtr->setWeightCKKWL(1.);
  mergingHooksPtr->setWeightFIRST(0.);

  // Reset weight of the event.
  double wgt      = 1.;
  // Reset the O(alphaS)-term of the UMEPS weight.
  double wgtFIRST = 0.;
  mergingHooksPtr->muMI(-1.);

  // Check if event passes the merging scale cut.
  double tmsval  = mergingHooksPtr->tms();

  if (doMOPS) tmsval = 0.;

  // Calculate number of clustering steps
  int nSteps = mergingHooksPtr->getNumberOfClusteringSteps(myHistory->state);
  mergingHooksPtr->nHardNowSave = nSteps;

  // Get merging scale in current event.
  double tmsnow  = mergingHooksPtr->tmsNow( myHistory->state );
  int nRequested = mergingHooksPtr->nRequested();

  if (doMOPS && nSteps == 0) { return 1; }

  // Too few steps can be possible if a chain of resonance decays has been
  // removed. In this case, reject this event, since it will be handled in
  // lower-multiplicity samples.
  if (nSteps < nRequested) {
    string message="Warning in DireMerging::calculateWeights: Les Houches Event";
    message+=" after removing decay products does not contain enough partons.";
    infoPtr->errorMsg(message);
    if (allowReject) return -1;
  }

  // Reset the minimal tms value, if necessary.
  tmsNowMin = (nSteps == 0) ? 0. : min(tmsNowMin, tmsnow);

  // Do not apply cut if the configuration could not be projected onto an
  // underlying born configuration.
  bool applyCut = nSteps > 0 && myHistory->select(RNpath)->nClusterings(NULL) > 0;

  // Enfore merging scale cut if the event did not pass the merging scale
  // criterion.
  if ( enforceCutOnLHE && applyCut && nSteps == nRequested
    && tmsnow < tmsval && tmsval > 0.) {
    string message="Warning in DireMerging::calculateWeights: Les Houches";
    message+=" Event fails merging scale cut. Reject event.";
    infoPtr->errorMsg(message);
    if (allowReject) return -1;
  }

  // Potentially recluster real emission jets for powheg input containing
  // "too many" jets, i.e. real-emission kinematics.
  bool containsRealKin = nSteps > nRequested && nSteps > 0;
  if ( containsRealKin ) nRecluster += nSteps - nRequested;

  // Remove real emission events without underlying Born configuration from
  // the loop sample, since such states will be taken care of by tree-level
  // samples.
  if ( doUNLOPSLoop && containsRealKin && !allowIncompleteReal
    && myHistory->select(RNpath)->nClusterings(NULL) == 0 ) {
    if (allowReject) return -1;
  }

  // Discard if the state could not be reclustered to any state above TMS.
  int nPerformed = 0;
  if ( nSteps > 0 && !allowIncompleteReal
    && ( doUNLOPSSubt || doUNLOPSSubtNLO || containsRealKin )
    && !myHistory->getFirstClusteredEventAboveTMS( RNpath, nRecluster,
          myHistory->state, nPerformed, false ) ) {
    if (allowReject) return -1;
    //return -1;
  }

  // Check reclustering steps to correctly apply MPI.
  mergingHooksPtr->nMinMPI(nSteps - nPerformed);

  // Perform one reclustering for real emission kinematics, then apply
  // merging scale cut on underlying Born kinematics.
  if ( containsRealKin ) {
    Event dummy = Event();
    // Initialise temporary output of reclustering.
    dummy.clear();
    dummy.init( "(hard process-modified)", particleDataPtr );
    dummy.clear();
    // Recluster once.
    myHistory->getClusteredEvent( RNpath, nSteps, dummy );
    double tnowNew  = mergingHooksPtr->tmsNow( dummy );
    // Veto if underlying Born kinematics do not pass merging scale cut.
    if ( enforceCutOnLHE && nSteps > 0 && nRequested > 0
      && tnowNew < tmsval && tmsval > 0.) {
      string message="Warning in DireMerging::calculateWeights: Les Houches";
      message+=" Event fails merging scale cut. Reject event.";
      infoPtr->errorMsg(message);
      if (allowReject) return -1;
    }
  }

  // Setup to choose shower starting conditions randomly.
  double sumAll(0.), sumFullAll(0.);
  for ( map<double, DireHistory*>::iterator it = myHistory->branches.begin();
    it != myHistory->branches.end(); ++it ) {
    sumAll     += it->second->prodOfProbs;
    sumFullAll += it->second->prodOfProbsFull;
  }

  // Sum up signal and background probabilities.
  double sumSignalProb(0.), sumBkgrndProb(0.);

  // New UNLOPS strategy based on UN2LOPS.
  bool doUNLOPS2 = false;
  int depth = (!doUNLOPS2) ? -1 : ( (containsRealKin) ? nSteps-1 : nSteps);

  if (!useAll) {

  // Calculate weights.
  if (doMOPS)
    wgt = myHistory->weightMOPS( trialPartonLevelPtr,
            mergingHooksPtr->AlphaS_FSR(), mergingHooksPtr->AlphaEM_FSR(),
            RNpath);
  else if ( mergingHooksPtr->doCKKWLMerging() )
    wgt = myHistory->weightTREE( trialPartonLevelPtr,
            mergingHooksPtr->AlphaS_FSR(), mergingHooksPtr->AlphaS_ISR(),
            mergingHooksPtr->AlphaEM_FSR(), mergingHooksPtr->AlphaEM_ISR(),
            RNpath);
  else if (  mergingHooksPtr->doUMEPSTreeSave )
    wgt = myHistory->weight_UMEPS_TREE( trialPartonLevelPtr,
            mergingHooksPtr->AlphaS_FSR(), mergingHooksPtr->AlphaS_ISR(),
            mergingHooksPtr->AlphaEM_FSR(), mergingHooksPtr->AlphaEM_ISR(),
            RNpath);
  else if ( mergingHooksPtr->doUMEPSSubtSave )
    wgt = myHistory->weight_UMEPS_SUBT( trialPartonLevelPtr,
            mergingHooksPtr->AlphaS_FSR(), mergingHooksPtr->AlphaS_ISR(),
            mergingHooksPtr->AlphaEM_FSR(), mergingHooksPtr->AlphaEM_ISR(),
            RNpath);
  else if ( mergingHooksPtr->doUNLOPSTreeSave )
    wgt = myHistory->weight_UNLOPS_TREE( trialPartonLevelPtr,
            mergingHooksPtr->AlphaS_FSR(), mergingHooksPtr->AlphaS_ISR(),
            mergingHooksPtr->AlphaEM_FSR(), mergingHooksPtr->AlphaEM_ISR(),
            RNpath, depth);
  else if ( mergingHooksPtr->doUNLOPSLoopSave )
    wgt = myHistory->weight_UNLOPS_LOOP( trialPartonLevelPtr,
            mergingHooksPtr->AlphaS_FSR(), mergingHooksPtr->AlphaS_ISR(),
            mergingHooksPtr->AlphaEM_FSR(), mergingHooksPtr->AlphaEM_ISR(),
            RNpath, depth);
  else if ( mergingHooksPtr->doUNLOPSSubtNLOSave )
    wgt = myHistory->weight_UNLOPS_SUBTNLO( trialPartonLevelPtr,
            mergingHooksPtr->AlphaS_FSR(), mergingHooksPtr->AlphaS_ISR(),
            mergingHooksPtr->AlphaEM_FSR(), mergingHooksPtr->AlphaEM_ISR(),
            RNpath, depth);
  else if ( mergingHooksPtr->doUNLOPSSubtSave )
    wgt = myHistory->weight_UNLOPS_SUBT( trialPartonLevelPtr,
            mergingHooksPtr->AlphaS_FSR(), mergingHooksPtr->AlphaS_ISR(),
            mergingHooksPtr->AlphaEM_FSR(), mergingHooksPtr->AlphaEM_ISR(),
            RNpath, depth);

  // For tree-level or subtractive sammples, rescale with k-Factor
  if ( doUNLOPSTree || doUNLOPSSubt ){
    // Find k-factor
    double kFactor = 1.;
    if ( nSteps > mergingHooksPtr->nMaxJetsNLO() )
      kFactor = mergingHooksPtr->kFactor( mergingHooksPtr->nMaxJetsNLO() );
    else kFactor = mergingHooksPtr->kFactor(nSteps);
    // For NLO merging, rescale CKKW-L weight with k-factor
    wgt *= (nRecluster == 2 && nloTilde) ? 1. : kFactor;
  }

  } else if (useAll && doMOPS) {
    // Calculate CKKWL reweighting for all paths.
    double wgtsum(0.);
    double lastp(0.);

    for ( map<double, DireHistory*>::iterator it = myHistory->branches.begin();
      it != myHistory->branches.end(); ++it ) {

      // Double to access path.
      double indexNow =  (lastp + 0.5*(it->first - lastp))/sumAll;
      lastp = it->first;

      // Probability of path.
      double probPath = it->second->prodOfProbsFull/sumFullAll;

      myHistory->select(indexNow)->setSelectedChild();

      // Calculate CKKWL weight:
      double w = myHistory->weightMOPS( trialPartonLevelPtr,
        mergingHooksPtr->AlphaS_FSR(), mergingHooksPtr->AlphaEM_FSR(),
        indexNow);

      wgtsum += probPath*w;

      // Sum up signal and background probabilities separately.
      if (it->second->hasTag("higgs"))
        sumSignalProb += it->second->prodOfProbsFull*w;
      else if (it->second->hasTag("bsm"))
        sumSignalProb += it->second->prodOfProbsFull*w;
      else
        sumBkgrndProb += it->second->prodOfProbsFull*w;

    }
    wgt = wgtsum;

  }

  mergingHooksPtr->setWeightCKKWL(wgt);

  // Check if we need to subtract the O(\alpha_s)-term. If the number
  // of additional partons is larger than the number of jets for
  // which loop matrix elements are available, do standard UMEPS.
  int nMaxNLO     = mergingHooksPtr->nMaxJetsNLO();
  bool doOASTree  = doUNLOPSTree && nSteps <= nMaxNLO;
  bool doOASSubt  = doUNLOPSSubt && nSteps <= nMaxNLO+1 && nSteps > 0;

  // Now begin NLO part for tree-level events
  if ( doOASTree || doOASSubt ) {

    // Decide on which order to expand to.
    int order = ( nSteps > 0 && nSteps <= nMaxNLO) ? 1 : -1;

    // Exclusive inputs:
    // Subtract only the O(\alpha_s^{n+0})-term from the tree-level
    // subtraction, if we're at the highest NLO multiplicity (nMaxNLO).
    if ( nloTilde && doUNLOPSSubt && nRecluster == 1
      && nSteps == nMaxNLO+1 ) order = 0;

    // Exclusive inputs:
    // Do not remove the O(as)-term if the number of reclusterings
    // exceeds the number of NLO jets, or if more clusterings have
    // been performed.
    if (nloTilde && doUNLOPSSubt && ( nSteps > nMaxNLO+1
      || (nSteps == nMaxNLO+1 && nPerformed != nRecluster) ))
        order = -1;

    // Calculate terms in expansion of the CKKW-L weight.
    wgtFIRST = myHistory->weight_UNLOPS_CORRECTION( order,
      trialPartonLevelPtr, mergingHooksPtr->AlphaS_FSR(),
      mergingHooksPtr->AlphaS_ISR(), mergingHooksPtr->AlphaEM_FSR(),
      mergingHooksPtr->AlphaEM_ISR(), RNpath, rndmPtr );

    // Exclusive inputs:
    // Subtract the O(\alpha_s^{n+1})-term from the tree-level
    // subtraction, not the O(\alpha_s^{n+0})-terms.
    if ( nloTilde && doUNLOPSSubt && nRecluster == 1
      && nPerformed == nRecluster && nSteps <= nMaxNLO )
      wgtFIRST += 1.;

    // Subtract the O(\alpha_s)-term from the CKKW-L weight
    // If PDF contributions have not been included, subtract these later
    // New UNLOPS based on UN2LOPS.
    if (doUNLOPS2 && order > -1) wgt = -wgt*(wgtFIRST-1.);
    else if (order > -1) wgt = wgt - wgtFIRST;

  }

  // If no-emission probability is zero.
  if (allowReject && wgt == 0.) return 0;

  // Done
  return 1;

}

//--------------------------------------------------------------------------

// Function to perform UNLOPS merging on this event.

int DireMerging::getStartingConditions( double RNpath, Event& process) {

  // Initialise which part of UNLOPS merging is applied.
  bool doUNLOPSSubt     = settingsPtr->flag("Merging:doUNLOPSSubt");
  bool doUNLOPSSubtNLO  = settingsPtr->flag("Merging:doUNLOPSSubtNLO");
  // Save number of clustering steps
  mergingHooksPtr->nReclusterSave = settingsPtr->mode("Merging:nRecluster");
  int nRecluster        = settingsPtr->mode("Merging:nRecluster");

  // Calculate number of clustering steps
  int nSteps = mergingHooksPtr->getNumberOfClusteringSteps(myHistory->state);
  mergingHooksPtr->nHardNowSave = nSteps;
  int nRequested = mergingHooksPtr->nRequested();

  // Potentially recluster real emission jets for powheg input containing
  // "too many" jets, i.e. real-emission kinematics.
  bool containsRealKin = nSteps > nRequested && nSteps > 0;
  if ( containsRealKin ) nRecluster += nSteps - nRequested;

  // Event with production scales set for further (trial) showering
  // and starting conditions for the shower.
  int nPerformed = 0;
  if (!doUNLOPSSubt && !doUNLOPSSubtNLO && !containsRealKin )
    myHistory->getStartingConditions(RNpath, process);
  // Do reclustering steps.
  else myHistory->getFirstClusteredEventAboveTMS( RNpath, nRecluster, process,
    nPerformed, true );

  // Set QCD 2->2 starting scale different from arbitrary scale in LHEF!
  // --> Set to minimal mT of partons.
  int nFinal = 0;
  double muf = process[0].e();
  for ( int i=0; i < process.size(); ++i )
  if ( process[i].isFinal()
    && (process[i].colType() != 0 || process[i].id() == 22 ) ) {
    nFinal++;
    muf = min( muf, abs(process[i].mT()) );
  }
  // For pure QCD dijet events (only!), set the process scale to the
  // transverse momentum of the outgoing partons.
  if ( nSteps == 0 && nFinal == 2
    && ( mergingHooksPtr->getProcessString().compare("pp>jj") == 0
      || mergingHooksPtr->getProcessString().compare("pp>aj") == 0) )
    process.scale(muf);

  // Reset hard process candidates (changed after clustering a parton).
  mergingHooksPtr->storeHardProcessCandidates( process );

  // Check if resonance structure has been changed
  //  (e.g. because of clustering W/Z/gluino)
  vector <int> oldResonance;
  for ( int i=0; i < myHistory->state.size(); ++i )
    if ( myHistory->state[i].status() == 22 )
      oldResonance.push_back(myHistory->state[i].id());
  vector <int> newResonance;
  for ( int i=0; i < process.size(); ++i )
    if ( process[i].status() == 22 )
      newResonance.push_back(process[i].id());
  // Compare old and new resonances
  for ( int i=0; i < int(oldResonance.size()); ++i )
    for ( int j=0; j < int(newResonance.size()); ++j )
      if ( newResonance[j] == oldResonance[i] ) {
        oldResonance[i] = 99;
        break;
      }
  bool hasNewResonances = (newResonance.size() != oldResonance.size());
  for ( int i=0; i < int(oldResonance.size()); ++i )
    hasNewResonances = (oldResonance[i] != 99);

  // If necessary, reattach resonance decay products.
  if (!hasNewResonances) evtUtils->reattachDecaysAfterMerging(process);

  // Allow merging hooks to remove emissions from now on.
  mergingHooksPtr->doIgnoreEmissions(false);

  // If the resonance structure of the process has changed due to reclustering,
  // redo the resonance decays in Pythia::next()
  if (hasNewResonances) return 2;

  // Done
  return 1;

}

//--------------------------------------------------------------------------

// Function to apply the merging scale cut on an input event.

bool DireMerging::cutOnProcess( Event& process) {

  // No cuts for MOPS.
  if (doMOPS) return false;

  bool useAll = true;

  bool doUMEPS      = settingsPtr->flag("Merging:doUMEPSTree")
                   || settingsPtr->flag("Merging:doUMEPSSubt");
  //bool doUNLOPSLoop = settingsPtr->flag("Merging:doUNLOPSLoop")
  //                 || settingsPtr->flag("Merging:doUNLOPSSubtNLO");
  // Save number of clustering steps
  int nReclusterSave = settingsPtr->mode("Merging:nRecluster");
  mergingHooksPtr->nReclusterSave = nReclusterSave;
  if (doUMEPS) mergingHooksPtr->nReclusterSave = 0;

  // For now, prefer construction of ordered histories.
  //mergingHooksPtr->orderHistories(true);
  //if (doUNLOPSLoop) mergingHooksPtr->orderHistories(false);
  mergingHooksPtr->orderHistories(false);
  // For pp > h, allow cut on state, so that underlying processes
  // can be clustered to gg > h
  if ( mergingHooksPtr->getProcessString().compare("pp>h") == 0)
    mergingHooksPtr->allowCutOnRecState(true);

  // Prepare process record for merging. If Pythia has already decayed
  // resonances used to define the hard process, remove resonance decay
  // products.
  Event newProcess( evtUtils->eventForMerging( process, true) );
  // Store candidates for the splitting V -> qqbar'
  mergingHooksPtr->storeHardProcessCandidates( newProcess );

  // Calculate number of clustering steps
  int nSteps = mergingHooksPtr->getNumberOfClusteringSteps(newProcess);
  mergingHooksPtr->nHardNowSave = nSteps;

  // Too few steps can be possible if a chain of resonance decays has been
  // removed. In this case, reject this event, since it will be handled in
  // lower-multiplicity samples.
  int nRequested = mergingHooksPtr->nRequested();
  // Potentially recluster real emission jets for powheg input containing
  // "too many" jets, i.e. real-emission kinematics.
  bool containsRealKin = nSteps > nRequested && nSteps > 0;

  // Check if event passes the merging scale cut.
  double tmsval  = mergingHooksPtr->tms();
  // Get merging scale in current event.
  double tmsnow  = !containsRealKin ? mergingHooksPtr->tmsNow(newProcess) : 0.; 
  int vetoCode = mergingHooksPtr->ktTypeSave;
  mergingHooksPtr->ktTypeSave=-99;

  if (nSteps < nRequested) return true;

  // Reset the minimal tms value, if necessary.
  tmsNowMin = (nSteps == 0) ? 0. : min(tmsNowMin, tmsnow);

  // Set dummy process scale.
  newProcess.scale(0.0);
  // Generate all histories
  DireHistory FullHistory( nSteps, 0.0, newProcess, DireClustering(),
    mergingHooksPtr, (*beamAPtr), (*beamBPtr), particleDataPtr, infoPtr,
    trialPartonLevelPtr, fsr, isr, psweights, coupSMPtr, true, true,
    containsRealKin, 1.0, 1.0, 1.0, 1.0, 0);

  // If need be, perform one clustering of real-emission state to one
  // underlying Born, with uniform probability
  DireHistory* picked = &FullHistory;
  if ( containsRealKin && !FullHistory.children.empty()) {
    picked = FullHistory.getLeafDemocratic(rndmPtr);
    picked->rescaleProbs();
    picked->mother = 0;
    picked->projectOntoDesiredHistories();
  } else {
    // Project histories onto desired branches, e.g. only ordered paths.
    picked->projectOntoDesiredHistories();
  }

  // Setup to choose shower starting conditions randomly.
  double sumAll(0.), sumFullAll(0.);
  double RN = picked->getPathIndexRandom(rndmPtr, true, sumAll,
     sumFullAll);

  // Pick path according to probability: Overwrite random number! 
  if (!useAll) RN = rndmPtr->flat();

  // Now enfore merging scale cut if the input event did not pass the merging
  // scale. Note: Do not apply cut if the configuration could not be projected
  // onto an underlying born configuration.
  bool doVetoInputState = !containsRealKin
   && nSteps > 0
   && FullHistory.select(RN)->nClusterings(NULL) > 0
   && nSteps == nRequested
   && tmsnow < tmsval;
  // If merging scale is not well-defined, check veto code instead!
  if (tmsnow <= 0. && vetoCode!=-99) {
    if (vetoCode == -1) doVetoInputState = !containsRealKin
      && nSteps > 0
      && FullHistory.select(RN)->nClusterings(NULL) > 0
      && nSteps == nRequested;
    else                doVetoInputState = false;
  }

  if (doVetoInputState) {
    string message="Warning in DireMerging::cutOnProcess: Input event";
    message+=" fails merging scale cut. Reject event.";
    infoPtr->errorMsg(message);
    return true;
  }

  // Remove real emission events without underlying Born configuration from
  // the loop sample, since such states will be taken care of by tree-level
  // samples.
  if ( containsRealKin && !allowIncompleteReal
    //&& nSteps > 1 && picked->select(RN)->nClusterings(NULL) == 0) {
    && nSteps > 1 && picked == &FullHistory) {
    string message="Warning in DireMerging::cutOnProcess:";
    message+=" real-correction event has no underlying Born. Reject.";
    infoPtr->errorMsg(message);
    return true;
  }

  // Perform one reclustering for real emission kinematics, then apply
  // merging scale cut on underlying Born kinematics.
  if (containsRealKin) {
    mergingHooksPtr->nReclusterSave = 1;
    double tnowNew  = mergingHooksPtr->tmsNow( picked->state);
    bool doVetoClusteredState = nSteps > 0 && nRequested > 0
      && tnowNew < tmsval;
    vetoCode = mergingHooksPtr->ktTypeSave;
    mergingHooksPtr->ktTypeSave=-99;
    if (vetoCode==-99) doVetoClusteredState = false;
    if (tnowNew <= 0. && vetoCode!=-99) {
      if (vetoCode == -1) doVetoClusteredState = nSteps > 0 && nRequested > 0;
      else                doVetoClusteredState = false;
    }
    mergingHooksPtr->nReclusterSave = nReclusterSave;
    // Veto if underlying Born kinematics do not pass merging scale cut.
    if (doVetoClusteredState) {
      string message="Warning in DireMerging::cutOnProcess: Clustered";
      message+=" input event fails merging scale cut. Reject event.";
      infoPtr->errorMsg(message);
      return true;
    }
  }

  mergingHooksPtr->nReclusterSave = nReclusterSave;

  // Done if only interested in cross section estimate after cuts.
  return false;

}

//==========================================================================

} // end namespace Pythia8
